<?php
/**
 * Template for displaying search forms in verito
 * @package WordPress
 * @subpackage verito
 * @since verito 1.0
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo esc_attr_e( 'Search for:','verito' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_e( 'Search &hellip;',  'verito' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="search-submit"><span class="screen-reader-text"><?php echo esc_attr_e( 'Search', 'verito' ); ?></span></button>
</form>
