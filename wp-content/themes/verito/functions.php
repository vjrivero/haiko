<?php
/*Define Constants */
define('VERITO_VERSION', '1.0');  
define('VERITO_THEME_PATH', get_template_directory());
define('VERITO_THEME_URI', get_template_directory_uri());
define('VERITO_THEME_NAME', 'verito');

/* Include required tgm activation */
require_once ( get_template_directory(). '/includes/tgm_activation/install-required.php');
require_once ( get_template_directory(). '/includes/reduxActivate.php');
if (file_exists( get_template_directory(). '/includes/reduxConfig.php')) {
    require_once ( get_template_directory(). '/includes/reduxConfig.php');
}

/* Include theme variation functions */   
require_once(VERITO_THEME_PATH . '/core/mgk_framework.php');


if (!isset($content_width)) {
    $content_width = 800;
}



class Verito {
   
  /**
  * Constructor
  */
  function __construct() {
    // Register action/filter callbacks
  
    add_action('after_setup_theme', array($this, 'verito_setup'));
    add_action( 'init', array($this, 'verito_theme'));
    add_action('wp_enqueue_scripts', array($this,'verito_custom_enqueue_google_font'));
    
    add_action('admin_enqueue_scripts', array($this,'verito_admin_scripts_styles'));
    add_action('wp_enqueue_scripts', array($this,'verito_scripts_styles'));


    add_action('widgets_init', array($this,'verito_widgets_init'));
    add_action('wp_enqueue_scripts', array($this,'verito_enqueue_custom_css'));
    
    add_action('add_meta_boxes', array($this,'verito_reg_page_meta_box'));
    add_action('save_post',array($this, 'verito_save_page_layout_meta_box_values')); 
    add_action('add_meta_boxes', array($this,'verito_reg_post_meta_box'));
    add_action('save_post',array($this, 'verito_save_post_layout_meta_box_values')); 
 
    }

    function verito_theme() {

    global $verito_Options;

    }

  /** * Theme setup */
  function verito_setup() {   
    global $verito_Options;
     load_theme_textdomain('verito', get_template_directory() . '/languages');
     load_theme_textdomain('woocommerce', get_template_directory() . '/languages');

      // Add default posts and comments RSS feed links to head.
      add_theme_support('automatic-feed-links');
      add_theme_support('title-tag');
      add_theme_support('post-thumbnails');
      add_image_size('verito-featured_preview', 55, 55, true);
      add_image_size('verito-article-home-large',1170, 450, true);
      add_image_size('verito-product-size-large',266, 323, true);      
          
         
    add_theme_support( 'html5', array(
      'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );
    
    add_theme_support( 'post-formats', array(
      'aside','video','audio'
    ) );
    
    // Setup the WordPress core custom background feature.
    $default_color = trim( 'ffffff', '#' );
    $default_text_color = trim( '333333', '#' );
    
    add_theme_support( 'custom-background', apply_filters( 'verito_custom_background_args', array(
      'default-color'      => $default_color,
      'default-attachment' => 'fixed',
    ) ) );
    
    add_theme_support( 'custom-header', apply_filters( 'verito_custom_header_args', array(
      'default-text-color'     => $default_text_color,
      'width'                  => 1170,
      'height'                 => 450,
      
    ) ) );

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, icons, and column width.
     */
    add_editor_style('css/editor-style.css' );
    
    /*
    * Edge WooCommerce Declaration: WooCommerce Support and settings
    */    
    
      if (class_exists('WooCommerce')) {
        add_theme_support('woocommerce');
        require_once(VERITO_THEME_PATH. '/woo_function.php');
        // Disable WooCommerce Default CSS if set
       
      }
 
    // Register navigation menus
    
    register_nav_menus(
      array(
      'toplinks' => esc_html__( 'Top menu', 'verito' ),
       'middlelinks' => esc_html__( 'Middle menu', 'verito' ),
       'main_menu' => esc_html__( 'Main menu', 'verito' )
      ));
    
  }
    

function verito_fonts_url() {
  global $verito_Options;
  $fonts_url = '';
  $fonts     = array();
  $subsets   = 'latin,latin-ext';
 
     if (isset($verito_Options['theme_layout']) && $verito_Options['theme_layout']=='version2')
    {
      if ( 'off' !== _x( 'on', 'Montserrat: on or off', 'verito' ) ) {
             $fonts[]='Montserrat:400,700';
            }
      
    }
    else
    {
      if ( 'off' !== _x( 'on', 'Dancing Script: on or off', 'verito' ) ) {
             $fonts[]='Dancing Script:400,700';
            } 
    }
   if ( 'off' !== _x( 'on', 'Open Sans: on or off', 'verito' ) ) {
         $fonts[]='Open Sans:300,400,600,700,800';
        }
  
  if ( 'off' !== _x( 'on', 'PT Sans: on or off', 'verito' ) ) {
         $fonts[]='PT Sans:400,700';
        }


    if ( $fonts ) {
    $fonts_url = add_query_arg( array(
      'family' => urlencode( implode( '|', $fonts ) ),
      'subset' => urlencode( $subsets ),
    ), 'https://fonts.googleapis.com/css' );
  }
    return $fonts_url;
}
/*
Enqueue scripts and styles.
*/
function verito_custom_enqueue_google_font() {

  wp_enqueue_style('verito-Fonts', $this->verito_fonts_url() , array(), '1.0.0' );
}


  function verito_admin_scripts_styles()
  {  
      wp_enqueue_script('verito-adminjs', VERITO_THEME_URI . '/js/admin_menu.js', array(), '', true);
      wp_enqueue_style('verito-adminmenu', VERITO_THEME_URI . '/css/admin_menu.css', array(), '');
  }

 
function verito_scripts_styles()
{
    global $verito_Options,$yith_wcwl;

     $woo_exist=false;
   if (class_exists('WooCommerce')) 
     {
     $woo_exist=true;
     }
    /*JavaScript for threaded Comments when needed*/
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

  wp_enqueue_style('bootstrap', VERITO_THEME_URI . '/css/bootstrap.min.css', array(), '');   
  wp_enqueue_style('font-awesome', VERITO_THEME_URI . '/css/font-awesome.css', array(), '');
  wp_enqueue_style('simple-line-icons', VERITO_THEME_URI . '/css/simple-line-icons.css', array(), '');

  wp_enqueue_style('owl.carousel', VERITO_THEME_URI . '/css/owl.carousel.css', array(), '');

  wp_enqueue_style('owl.theme', VERITO_THEME_URI . '/css/owl.theme.css', array(), '');
  
  wp_enqueue_style('flexslider', VERITO_THEME_URI . '/css/flexslider.css', array(), '');

   wp_enqueue_style('jquery.bxslider', VERITO_THEME_URI . '/css/jquery.bxslider.css', array(), '');
  
  wp_enqueue_style('verito-style', VERITO_THEME_URI . '/style.css', array(), '');    


    if (isset($verito_Options['theme_layout']) && !empty($verito_Options['theme_layout']))
     {
    wp_enqueue_style('verito-blog', VERITO_THEME_URI . '/skins/' . $verito_Options['theme_layout'] . '/blogs.css', array(), '');
    wp_enqueue_style('verito-revslider', VERITO_THEME_URI . '/skins/' . $verito_Options['theme_layout'] . '/revslider.css', array(), '');
    wp_enqueue_style('verito-layout', VERITO_THEME_URI . '/skins/' . $verito_Options['theme_layout'] . '/style.css', array(), '');
    wp_enqueue_style('verito-mgk_menu', VERITO_THEME_URI . '/skins/' . $verito_Options['theme_layout'] . '/mgk_menu.css', array(), '');  
    wp_enqueue_style('verito-jquery.mobile-menu', VERITO_THEME_URI . '/skins/' . $verito_Options['theme_layout'] . '/jquery.mobile-menu.css', array(), '');
     }
     else
     {
    wp_enqueue_style('verito-blog', VERITO_THEME_URI . '/skins/default/blogs.css', array(), '');
    wp_enqueue_style('verito-revslider', VERITO_THEME_URI . '/skins/default/revslider.css', array(), '');
    wp_enqueue_style('verito-layout', VERITO_THEME_URI . '/skins/default/style.css', array(), '');
    wp_enqueue_style('verito-mgk_menu', VERITO_THEME_URI . '/skins/default/mgk_menu.css', array(), '');  
    wp_enqueue_style('verito-jquery.mobile-menu', VERITO_THEME_URI . '/skins/default/jquery.mobile-menu.css', array(), '');
     }
    
 //theme js

    wp_enqueue_script('bootstrap', VERITO_THEME_URI . '/js/bootstrap.min.js', array('jquery'), '', true);
    wp_enqueue_script('jquery.cookie', VERITO_THEME_URI . '/js/jquery.cookie.min.js', array('jquery'), '', true);
    wp_enqueue_script('verito-countdown',VERITO_THEME_URI . '/js/countdown.js', array('jquery'), '', true);
    wp_enqueue_script('parallax',VERITO_THEME_URI . '/js/parallax.js', array('jquery'), '', true);
    wp_enqueue_script('revslider', VERITO_THEME_URI . '/js/revslider.js', array('jquery'), '', true);

    wp_enqueue_script('verito-common-js',VERITO_THEME_URI . '/js/common.js', array('jquery'), '', true);
        
        if (isset($yith_wcwl) && is_object($yith_wcwl)) { 
    wp_localize_script( 'verito-common-js', 'js_verito_wishvar', array(
            
            'MGK_ADD_TO_WISHLIST_SUCCESS_TEXT' => esc_html__('Product successfully added to wishlist','verito').' <a href="'.esc_url($yith_wcwl->get_wishlist_url()).'">'.esc_html__('Browse Wishlist.','verito').'</a>' ,
            'MGK_ADD_TO_WISHLIST_EXISTS_TEXT' => esc_html__('The product is already in the wishlist!','verito').' <a href="'.esc_url($yith_wcwl->get_wishlist_url()).'">'.esc_html__('Browse Wishlist.','verito').'</a>' ,
              'IMAGEURL' => esc_url(VERITO_THEME_URI).'/images',
              'WOO_EXIST' =>esc_html($woo_exist), 
              'SITEURL' => esc_url(site_url())
                     
        ) );
     }
     else
     {
           wp_localize_script( 'verito-common-js', 'js_verito_wishvar', array(
            
              'WOO_EXIST' =>esc_html($woo_exist), 
              'SITEURL' => esc_url(site_url())
                     
        ) );

     }
  
    wp_enqueue_script('jquery.mobile-menu', VERITO_THEME_URI . '/js/jquery.mobile-menu.min.js', array('jquery'), '', true);
    wp_enqueue_script('owl.carousel',VERITO_THEME_URI . '/js/owl.carousel.min.js', array('jquery'), '', true);
        
    wp_enqueue_script('verito-cloud-zoom-js', VERITO_THEME_URI . '/js/cloud-zoom.js', array('jquery'), '', true);
  
     wp_enqueue_script('verito-themejs', VERITO_THEME_URI .'/js/mgk_menu.js', array('jquery'), '', true );

            wp_localize_script('verito-themejs', 'js_verito_vars', array(
            'ajax_url' => esc_url(admin_url( 'admin-ajax.php' )),
            'container_width' => 1250,
            'grid_layout_width' => 20           
        ) );

 
           
}
  //register sidebar widget
  function verito_widgets_init()
  {
      register_sidebar(array(
      'name' => esc_html__('Blog Sidebar', 'verito'),
      'id' => 'sidebar-blog',
      'description' => esc_html__('Sidebar that appears on the right of Blog and Search page.', 'verito'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="block-title">',
      'after_title' => '</h3>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Shop Sidebar','verito'),
      'id' => 'sidebar-shop',
      'description' => esc_html__('Main sidebar that appears on the left.', 'verito'),
      'before_widget' => '<div id="%1$s" class="block %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Content Sidebar Left', 'verito'),
      'id' => 'sidebar-content-left',
      'description' => esc_html__('Additional sidebar that appears on the left.','verito'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Content Sidebar Right', 'verito'),
      'id' => 'sidebar-content-right',
      'description' => esc_html__('Additional sidebar that appears on the right.', 'verito'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
   
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 1','verito'),
      'id' => 'footer-sidebar-1',
      'description' => esc_html__('Appears in the footer section of the site.','verito'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 2', 'verito'),
      'id' => 'footer-sidebar-2',
      'description' => esc_html__('Appears in the footer section of the site.', 'verito'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 3', 'verito'),
      'id' => 'footer-sidebar-3',
      'description' => esc_html__('Appears in the footer section of the site.','verito'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 4', 'verito'),
      'id' => 'footer-sidebar-4',
      'description' => esc_html__('Appears in the footer section of the site.', 'verito'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 5', 'verito'),
      'id' => 'footer-sidebar-5',
      'description' => esc_html__('Appears in the footer section of the site.', 'verito'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));

  }



  function verito_reg_page_meta_box() {
    $screens = array('page');

    foreach ($screens as $screen) {        
      add_meta_box(
          'verito_page_layout_meta_box', esc_html__('Page Layout', 'verito'), 
          array($this, 'verito_page_layout_meta_box_cb'), $screen, 'normal', 'core'
      );
    }
  }

  function verito_page_layout_meta_box_cb($post) {

    $saved_page_layout = get_post_meta($post->ID, 'verito_page_layout', true);
    
    $show_breadcrumb = get_post_meta($post->ID, 'verito_show_breadcrumb', true);
    
   if(empty($saved_page_layout)) {
      $saved_page_layout = 3;
    }
    $page_layouts = array(
      1 => esc_url(VERITO_THEME_URI).'/images/magik_col/category-layout-1.png',
      2 => esc_url(VERITO_THEME_URI).'/images/magik_col/category-layout-2.png',
      3 => esc_url(VERITO_THEME_URI).'/images/magik_col/category-layout-3.png',
      4 => esc_url(VERITO_THEME_URI).'/images/magik_col/category-layout-4.png',
    );  
    ?>
  
  <?php
    echo "<input type='hidden' name='verito_page_layout_verifier' value='".wp_create_nonce('verito_7a81jjde')."' />";    
    $output = '<div class="tile_img_wrap">';
      foreach ($page_layouts as $key => $img) {
        $checked = '';
        $selectedClass = '';
        if($saved_page_layout == $key){
          $checked = 'checked="checked"';
          $selectedClass = 'of-radio-img-selected';
        }
        $output .= '<span>';
        $output .= '<input type="radio" class="checkbox of-radio-img-radio" value="' . absint($key) . '" name="verito_page_layout" ' . esc_html($checked). ' />';            
        $output .= '<img src="' . esc_url($img) . '" alt="" class="of-radio-img-img ' . esc_html($selectedClass) . '" />';
        $output .= '</span>';
            
      }    
    $output .= '</div>';
    echo htmlspecialchars_decode($output);
    ?>

  <h2><?php esc_attr_e('Show breadcrumb', 'verito'); ?></h2>
  <p>
    <input type="radio" name="verito_show_breadcrumb" value="1" <?php echo "checked='checked'"; ?> />
    <label><?php esc_attr_e('Yes','verito'); ?></label>
    &nbsp;
    <input type="radio" name="verito_show_breadcrumb" value="0"  <?php if($show_breadcrumb === '0'){ echo "checked='checked'"; } ?>/>
    <label><?php esc_attr_e('No', 'verito'); ?></label>
  </p>
  <?php
  }

  function verito_save_page_layout_meta_box_values($post_id){
    if (!isset($_POST['verito_page_layout_verifier']) 
        || !wp_verify_nonce($_POST['verito_page_layout_verifier'], 'verito_7a81jjde') 
        || !isset($_POST['verito_page_layout']) 
       
        )
      return $post_id;
    
    
    add_post_meta($post_id,'verito_page_layout',sanitize_text_field( $_POST['verito_page_layout']),true) or 
    update_post_meta($post_id,'verito_page_layout',sanitize_text_field( $_POST['verito_page_layout']));
    
    add_post_meta($post_id,'verito_show_breadcrumb',sanitize_text_field( $_POST['verito_show_breadcrumb']),true) or 
    update_post_meta($post_id,'verito_show_breadcrumb',sanitize_text_field( $_POST['verito_show_breadcrumb']));  
  }


  /*Register Post Meta Boxes for Blog Post Layouts*/

    function verito_reg_post_meta_box() {
    $screens = array('post');

    foreach ($screens as $screen) {        
      add_meta_box(
          'verito_post_layout_meta_box', esc_html__('Post Layout', 'verito'), 
          array($this, 'verito_post_layout_meta_box_cb'), $screen, 'normal', 'core'
      );
    }
  }

  function verito_post_layout_meta_box_cb($post) {

    $saved_post_layout = get_post_meta($post->ID, 'verito_post_layout', true);         
    if(empty($saved_post_layout))
    {
      $saved_post_layout = 2;
    }
    
    $post_layouts = array(
      1 => esc_url(VERITO_THEME_URI).'/images/magik_col/category-layout-1.png',
      2 => esc_url(VERITO_THEME_URI).'/images/magik_col/category-layout-2.png',
      3 => esc_url(VERITO_THEME_URI).'/images/magik_col/category-layout-3.png',
      
    );  
    ?>
  
  <?php
    echo "<input type='hidden' name='verito_post_layout_verifier' value='".wp_create_nonce('verito_7a81jjde1')."' />";    
    $output = '<div class="tile_img_wrap">';
      foreach ($post_layouts as $key => $img) {
        $checked = '';
        $selectedClass = '';
        if($saved_post_layout == $key){
          $checked = 'checked="checked"';
          $selectedClass = 'of-radio-img-selected';
        }
        $output .= '<span>';
        $output .= '<input type="radio" class="checkbox of-radio-img-radio" value="' . absint($key) . '" name="verito_post_layout" ' . esc_html($checked). ' />';            
        $output .= '<img src="' . esc_url($img) . '" alt="" class="of-radio-img-img ' . esc_html($selectedClass) . '" />';
        $output .= '</span>';
            
      }    
    $output .= '</div>';
    echo htmlspecialchars_decode($output);
    ?>

  
  <?php
  }

  function verito_save_post_layout_meta_box_values($post_id){
    if (!isset($_POST['verito_post_layout_verifier']) 
        || !wp_verify_nonce($_POST['verito_post_layout_verifier'], 'verito_7a81jjde1') 
        || !isset($_POST['verito_post_layout']) 
       
        )
      return $post_id;
    
    
    add_post_meta($post_id,'verito_post_layout',sanitize_text_field($_POST['verito_post_layout']),true) or 
    update_post_meta($post_id,'verito_post_layout',sanitize_text_field($_POST['verito_post_layout']));
    
    
  }

  //custom functions 



// page title code
function verito_page_title() {

    global  $post, $wp_query, $author,$verito_Options;

    $home = esc_html__('Home', 'verito');

  
    if ( ( ! is_home() && ! is_front_page() && ! (is_post_type_archive()) ) || is_paged() ) {

        if ( is_home() ) {
           echo htmlspecialchars_decode(single_post_title('', false));

        } else if ( is_category() ) {

            echo esc_html(single_cat_title( '', false ));

        } elseif ( is_tax() ) {

            $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

            echo htmlspecialchars_decode(esc_html( $current_term->name ));

        }  elseif ( is_day() ) {

            printf( esc_html__( 'Daily Archives: %s', 'verito' ), get_the_date() );

        } elseif ( is_month() ) {

            printf( esc_html__( 'Monthly Archives: %s', 'verito' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'verito' ) ) );

        } elseif ( is_year() ) {

            printf( esc_html__( 'Yearly Archives: %s', 'verito' ), get_the_date( _x( 'Y', 'yearly archives date format', 'verito' ) ) );

        }   else if ( is_post_type_archive() ) {
            sprintf( esc_html__( 'Archives: %s', 'verito' ), post_type_archive_title( '', false ) );
        } elseif ( is_single() && ! is_attachment() ) {
        
                echo esc_html(get_the_title());

            

        } elseif ( is_404() ) {

            echo esc_html__( 'Error 404', 'verito' );

        } elseif ( is_attachment() ) {

            echo esc_html(get_the_title());

        } elseif ( is_page() && !$post->post_parent ) {

            echo esc_html(get_the_title());

        } elseif ( is_page() && $post->post_parent ) {

            echo esc_html(get_the_title());

        } elseif ( is_search() ) {

            echo htmlspecialchars_decode(esc_html__( 'Search results for &ldquo;', 'verito' ) . get_search_query() . '&rdquo;');

        } elseif ( is_tag() ) {

            echo htmlspecialchars_decode(esc_html__( 'Posts tagged &ldquo;', 'verito' ) . single_tag_title('', false) . '&rdquo;');

        } elseif ( is_author() ) {

            $userdata = get_userdata($author);
            echo htmlspecialchars_decode(esc_html__( 'Author:', 'verito' ) . ' ' . $userdata->display_name);

        } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' ) {

            $post_type = get_post_type_object( get_post_type() );

            if ( $post_type ) {
                echo htmlspecialchars_decode($post_type->labels->singular_name);
            }

        }

        if ( get_query_var( 'paged' ) ) {
            echo htmlspecialchars_decode( ' (' . esc_html__( 'Page', 'verito' ) . ' ' . get_query_var( 'paged' ) . ')');
        }
    } else {
        if ( is_home() && !is_front_page() ) {
            if ( ! empty( $home ) ) {               
                  echo htmlspecialchars_decode(single_post_title('', false));
            }
        }
    }
}

// page breadcrumbs code
function verito_breadcrumbs() {
    global $post, $verito_Options,$wp_query, $author;

    $delimiter = '<span>&frasl; </span>';
    $before = '<li>';
    $after = '</li>';
    $home = esc_html__('Home', 'verito');
    $linkbefore='<strong>';
    $linkafter='</strong>';

  
  // breadcrumb code
   
    if ( ( ! is_home() && ! is_front_page() && ! (is_post_type_archive()) ) || is_paged() ) {
        echo '<ul>';

        if ( ! empty( $home ) ) {
            echo htmlspecialchars_decode($before . '<a class="home" href="' . esc_url(home_url('/')) . '">' . $home . '</a>' . $delimiter . $after);
        }

        if ( is_home() ) {

            echo htmlspecialchars_decode($before .$linkbefore. single_post_title('', false) .$linkafter. $after);

         }      
         else if ( is_category() ) {

            if ( get_option( 'show_on_front' ) == 'page' ) {
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
            }

            $cat_obj = $wp_query->get_queried_object();
            if ($cat_obj) {
                $this_category = get_category( $cat_obj->term_id );
                if ( 0 != $this_category->parent ) {
                    $parent_category = get_category( $this_category->parent );
                    if ( ( $parents = get_category_parents( $parent_category, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                        echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $delimiter . $after);
                    }
                }
                echo htmlspecialchars_decode($before .$linkbefore. single_cat_title( '', false ) .$linkafter. $after);
            }

        } 
        elseif ( is_tax()) {      
                    
            $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

            $ancestors = array_reverse( get_ancestors( $current_term->term_id, get_query_var( 'taxonomy' ) ) );

            foreach ( $ancestors as $ancestor ) {
                $ancestor = get_term( $ancestor, get_query_var( 'taxonomy' ) );

                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_term_link( $ancestor->slug, get_query_var( 'taxonomy' ) )) . '">' . esc_html( $ancestor->name ) . '</a>' . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before .$linkbefore. esc_html( $current_term->name ) .$linkafter. $after);

        } 
       
        elseif ( is_day() ) {

            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_month_link(get_the_time('Y'),get_the_time('m'))) . '">' . esc_html(get_the_time('F')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before .$linkbefore. get_the_time('d') .$linkafter. $after);

        } elseif ( is_month() ) {

            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before .$linkbefore. get_the_time('F') .$linkafter. $after);

        } elseif ( is_year() ) {

            echo htmlspecialchars_decode($before .$linkbefore. get_the_time('Y') .$linkafter. $after);

        } elseif ( is_single() && ! is_attachment() ) {

         
            if ( 'post' != get_post_type() ) {
                $post_type = get_post_type_object( get_post_type() );
                $slug = $post_type->rewrite;
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_post_type_archive_link( get_post_type() )) . '">' . esc_html($post_type->labels->singular_name) . '</a>' . $delimiter . $after);
                echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

            } else {

                if ( 'post' == get_post_type() && get_option( 'show_on_front' ) == 'page' ) {
                    echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
                }

                $cat = current( get_the_category() );
              if ( ( $parents = get_category_parents( $cat, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                $getitle=get_the_title();
                  if(empty($getitle))
                  {
                    $newdelimiter ='';
                  }
                  else
                  {
                     $newdelimiter=$delimiter;
                  }
                    echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $newdelimiter . $after);
                }
                echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

            }

        } elseif ( is_404() ) {

            echo htmlspecialchars_decode($before .$linkbefore. esc_html__( 'Error 404', 'verito' ) .$linkafter. $after);

        } elseif ( is_attachment() ) {

            $parent = get_post( $post->post_parent );
            $cat = get_the_category( $parent->ID );
            $cat = $cat[0];
            if ( ( $parents = get_category_parents( $cat, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $delimiter . $after);
            }
            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( $parent )) . '">' . esc_html($parent->post_title) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

        } elseif ( is_page() && !$post->post_parent ) {

            echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

        } elseif ( is_page() && $post->post_parent ) {

            $parent_id  = $post->post_parent;
            $breadcrumbs = array();

            while ( $parent_id ) {
                $page = get_post( $parent_id );
                $breadcrumbs[] = '<a href="' . esc_url(get_permalink($page->ID)) . '">' . esc_html(get_the_title( $page->ID )) . '</a>';
                $parent_id  = $page->post_parent;
            }

            $breadcrumbs = array_reverse( $breadcrumbs );

            foreach ( $breadcrumbs as $crumb ) {
                echo htmlspecialchars_decode($before . $crumb . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

        } elseif ( is_search() ) {

            echo htmlspecialchars_decode($before .$linkbefore. esc_html__( 'Search results for &ldquo;', 'verito' ) . get_search_query() . '&rdquo;' .$linkafter. $after);

        } elseif ( is_tag() ) {

            if ( 'post' == get_post_type() && get_option( 'show_on_front' ) == 'page' ) {
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before .$linkbefore. esc_html__( 'Posts tagged &ldquo;', 'verito' ) . single_tag_title('', false) . '&rdquo;' .$linkafter. $after);

        } elseif ( is_author() ) {

            $userdata = get_userdata($author);
            echo htmlspecialchars_decode($before .$linkbefore. esc_html__( 'Author:', 'verito' ) . ' ' . $userdata->display_name .$linkafter. $after);

        } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' ) {

            $post_type = get_post_type_object( get_post_type() );

            if ( $post_type ) {
                echo htmlspecialchars_decode($before .$linkbefore. $post_type->labels->singular_name .$linkafter. $after);
            }

        }

        if ( get_query_var( 'paged' ) ) {
            echo htmlspecialchars_decode($before .$linkbefore. '&nbsp;(' . esc_html__( 'Page', 'verito' ) . ' ' . get_query_var( 'paged' ) . ')' .$linkafter. $after);
        }

        echo '</ul>';
    } else { 
        if ( is_home() && !is_front_page() ) {
            echo '<ul>';

            if ( ! empty( $home ) ) {
                echo htmlspecialchars_decode($before . '<a class="home" href="' . esc_url(home_url('/')) . '">' . $home . '</a>' . $delimiter . $after);

               
                echo htmlspecialchars_decode($before .$linkbefore. single_post_title('', false) .$linkafter. $after);
            }

            echo '</ul>';
        }
    }
}
  

  function verito_getPostViews($postID)
  {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
      return "0  View";
    }
    return $count . '  Views';
  }

  function verito_setPostViews($postID)
  {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
    } else {
      $count++;
      update_post_meta($postID, $count_key, $count);
    }
  }


  function verito_is_blog() {
    global  $post;
    $posttype = get_post_type($post );
    return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
  }

 
 
 // comment display 
  function verito_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>

  <li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
    <div class="comment-body">
      <div class="img-thumbnail">
        <?php echo get_avatar($comment, 80); ?>
      </div>
      <div class="comment-block">
        <div class="comment-arrow"></div>
        <span class="comment-by">
          <strong><?php echo get_comment_author_link() ?></strong>
          <span class="pt-right">
            <span> <?php edit_comment_link('<i class="fa fa-pencil"></i> ' . esc_html__('Edit', 'verito'),'  ','') ?></span>
            <span> <?php comment_reply_link(array_merge( $args, array('reply_text' => '<i class="fa fa-reply"></i> ' . esc_html__('Reply', 'verito'), 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?></span>
          </span>
        </span>
        <div>
          <?php if ($comment->comment_approved == '0') : ?>
            <em><?php echo esc_html__('Your comment is awaiting moderation.', 'verito') ?></em>
            <br />
          <?php endif; ?>
          <?php comment_text() ?>
        </div>
        <span class="date pt-right"><?php printf(esc_html__('%1$s at %2$s', 'verito'), get_comment_date(),  get_comment_time()) ?></span>
      </div>
    </div>
  </li>
  <?php }

  //css manage by admin
  function verito_enqueue_custom_css() {
    global $verito_Options;

     wp_enqueue_style(
        'verito-custom-style',esc_url(VERITO_THEME_URI) . '/css/custom.css'
    );

   $custom_css='';
    ?>

      <?php if(isset($verito_Options['opt-color-rgba']) &&  !empty($verito_Options['opt-color-rgba'])) {
        $custom_css=
      ".mgk-main-menu {
        background-color: ". esc_html($verito_Options['opt-color-rgba'])." !important;
       }";  
      }

     
      if(isset($verito_Options['footer_color_scheme']) && $verito_Options['footer_color_scheme']) {
      if(isset($verito_Options['footer_copyright_background_color']) && !empty($verito_Options['footer_copyright_background_color'])) {
      
       $custom_css.=".footer-bottom {
        background-color: ". esc_html($verito_Options['footer_copyright_background_color'])." !important }";
       }
       

       if(isset($verito_Options['footer_copyright_font_color']) && !empty($verito_Options['footer_copyright_font_color'])) {
      $custom_css.=".coppyright {
        color: ". esc_html($verito_Options['footer_copyright_font_color'])." !important;}";    
       }     
       }
       

       
        if(isset($verito_Options['daily_deal_image']) && !empty($verito_Options['daily_deal_image']['url'])) {

          $custom_css.=".parallax-2
          {
             
              background:#f8f8f8 url('".esc_url($verito_Options['daily_deal_image']['url'])."') no-repeat bottom right;
          }";
          
           
         }
      
       if (isset($verito_Options['theme_layout']) && $verito_Options['theme_layout']=='version2')
     { 
       if (isset($verito_Options['enable_home_background_image']) && $verito_Options['enable_home_background_image']){
        
        $custom_css.="html {background:url('".esc_url($verito_Options['background_image']['url'])."') no-repeat top left; background-attachment: fixed;
         background-size: cover;} ";

      
       }
     }


       wp_add_inline_style( 'verito-custom-style', $custom_css );

  }
}

// Instantiate theme
$Verito = new Verito();

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_postcode']);
    unset($fields['shipping']['shipping_postcode']);
    return $fields;
}

add_filter( 'woocommerce_states', 'custom_woocommerce_states' );

function custom_woocommerce_states( $states ) {

$states['CO'] = array(

'AB' => __( 'Ábrego (Norte Sant.)', 'woocommerce' ),
'AC' => __( 'Acacias (Meta)', 'woocommerce' ),
'AG' => __( 'Aguachica (Cesar)', 'woocommerce' ),
'AL' => __( 'Albania-Cerrejon (Guajira)', 'woocommerce' ),
'AH' => __( 'Altamira (Huila)', 'woocommerce' ),
'AV' => __( 'Alvarado (Tolima)', 'woocommerce' ),
'AM' => __( 'Amaime (Valle)', 'woocommerce' ),
'AT' => __( 'Ambalema (Tolima)', 'woocommerce' ),
'AZ' => __( 'Andalucía (Valle)', 'woocommerce' ),
'AN' => __( 'Anserma Caldas', 'woocommerce' ),
'AU' => __( 'Anserma Nuevo (Valle)', 'woocommerce' ), 
'AP' => __( 'Apartado (Antioquia)', 'woocommerce' ),
'AY' => __( 'Apiay (Meta)', 'woocommerce' ),
'AR' => __( 'Aracataca (Magdalena)', 'woocommerce' ), 
'AS' => __( 'Aratoca (Santander)', 'woocommerce' ),
'AD' => __( 'Arauca (Caldas)', 'woocommerce' ),
'AO' => __( 'Arcabuco (Boyacá)', 'woocommerce' ),
'AI' => __( 'Armero (Tolima)', 'woocommerce' ), 
'BA' => __( 'Baranoa (Atlántico)', 'woocommerce' ),
'BB' => __( 'Barbosa (Santander)', 'woocommerce' ), 
'BC' => __( 'Barichara (Santander)', 'woocommerce' ),
'BD' => __( 'Barrancabermeja (Santander)', 'woocommerce' ),
'BE' => __( 'Barrancas (Guajira)', 'woocommerce' ), 
'BF' => __( 'Barranquilla (Atlántico)', 'woocommerce' ),
'BG' => __( 'Becerril (Cesar)', 'woocommerce' ), 
'BO' => __( 'Bogotá D.C. (Cundinamarca)', 'woocommerce' ),
'BH' => __( 'Bosconia (Cesar)', 'woocommerce' ),
'BI' => __( 'Bucaramanga (Santander)', 'woocommerce' ),
'BJ' => __( 'Buenaventura (Valle)', 'woocommerce' ), 
'BK' => __( 'Buenavista (Córdoba)', 'woocommerce' ), 
'BU' => __( 'Buga (Valle)', 'woocommerce' ), 
'BV' => __( 'Bugalagrande (Valle)', 'woocommerce' ),
'CA' => __( 'Caicedonia (Valle)', 'woocommerce' ),
'CB' => __( 'Cajamarca (Tolima)', 'woocommerce' ), 
'CC' => __( 'Calamar (Bolívar)', 'woocommerce' ),
'CD' => __( 'Cali (Valle)', 'woocommerce' ), 
'CE' => __( 'Caloto (Cauca)', 'woocommerce' ), 
'CF' => __( 'Campeche (Atlántico)', 'woocommerce' ),
'CG' => __( 'Campo de la Cruz (Atlántico)', 'woocommerce' ),
'CN' => __( 'Campoalegre (Huila)', 'woocommerce' ),
'CI' => __( 'Candelaria (Atlántico)', 'woocommerce' ),
'CJ' => __( 'Cartagena (Bolívar)', 'woocommerce' ),
'CK' => __( 'Castilla (Tolima)', 'woocommerce' ),
'CL' => __( 'Caucasia (Antioquia)', 'woocommerce' ),
'CM' => __( 'Cerete (Córdoba)', 'woocommerce' ), 
'CH1' => __( 'Chaparral (Tolima)', 'woocommerce' ), 
'CH2' => __( 'Charala (Santander)', 'woocommerce' ),
'CH3' => __( 'Chicoral (Tolima)', 'woocommerce' ), 
'CH4' => __( 'Chigorodo (Antioquia)', 'woocommerce' ),
'CH5' => __( 'Chinu (Córdoba)', 'woocommerce' ), 
'CH6' => __( 'Chiquinquirá (Boyacá)', 'woocommerce' ), 
'CH7' => __( 'Choconta (Cundinamarca)', 'woocommerce' ), 
'CU' => __( 'Ciénaga (Magdalena)', 'woocommerce' ), 
'CV' => __( 'CIénaga de oro (Córdoba)', 'woocommerce' ), 
'CW' => __( 'Cisneros (Valle)', 'woocommerce' ), 
'CX' => __( 'Codazzi (Cesar)', 'woocommerce' ), 
'CY' => __( 'Convención (Norte Sant.)', 'woocommerce' ),
'CZ' => __( 'Copey (Cesar)', 'woocommerce' ), 
'CO' => __( 'Corinto (Cauca)', 'woocommerce' ),
'CP' => __( 'Coveñas (Sucre)', 'woocommerce' ),
'CQ' => __( 'Coyaima (Tolima)', 'woocommerce' ), 
'CR' => __( 'Cúcuta (Norte Sant.)', 'woocommerce' ),
'CS' => __( 'Curiti (Santander)', 'woocommerce' ), 
'DA' => __( 'Dagua (Valle)', 'woocommerce' ),
'DI' => __( 'Distracción (Guajira)', 'woocommerce' ), 
'DU' => __( 'Duitama (Boyacá)', 'woocommerce' ),
'EB' => __( 'El Banco (Magdalena)', 'woocommerce' ), 
'EC' => __( 'El Cabuyal (Valle)', 'woocommerce' ), 
'ER' => __( 'El Carmen de Bolívar (Bolívar)', 'woocommerce' ),
'ET' => __( 'El Cerrito (Valle)', 'woocommerce' ), 
'EV' => __( 'El Convenio (Tolima)', 'woocommerce' ), 
'EH' => __( 'El Hormiguero (Cauca)', 'woocommerce' ), 
'EM' => __( 'El Molino (Guajira)', 'woocommerce' ), 
'EP' => __( 'El Playón Santander', 'woocommerce' ), 
'ED' => __( 'El Rodadero (Magdalena)', 'woocommerce' ), 
'EU' => __( 'El Totumo (Tolima)', 'woocommerce' ), 
'ES' => __( 'Espinal (Tolima)', 'woocommerce' ), 
'FA' => __( 'Flandes (Tolima)', 'woocommerce' ), 
'FO' => __( 'Florencia (Caquetá)', 'woocommerce' ),
'FL' => __( 'Florida (Valle)', 'woocommerce' ), 
'FN' => __( 'Fonseca (Guajira)', 'woocommerce' ), 
'FR' => __( 'Fresno (Tolima)', 'woocommerce' ), 
'FU' => __( 'Fundación (Magdalena)', 'woocommerce' ), 
'FS' => __( 'Fusagasugá (Cundinamarca)', 'woocommerce' ),
'GA' => __( 'Gaira (Magdalena)', 'woocommerce' ), 
'GM' => __( 'Gamarra César', 'woocommerce' ), 
'GG' => __( 'Garagoa (Boyacá)', 'woocommerce' ), 
'GZ' => __( 'Garzón (Huila)', 'woocommerce' ), 
'GT' => __( 'Gigante (Huila)', 'woocommerce' ), 
'GB' => __( 'Ginebra (Valle)', 'woocommerce' ), 
'GD' => __( 'Girardot (Cundinamarca)', 'woocommerce' ), 
'GR' => __( 'Granada (Meta)', 'woocommerce' ), 
'GU' => __( 'Guacari (Valle)', 'woocommerce' ), 
'GY' => __( 'Gualanday (Tolima)', 'woocommerce' ), 
'GL' => __( 'Guamal (Meta)', 'woocommerce' ), 
'GO' => __( 'Guamo (Tolima)', 'woocommerce' ), 
'GQ' => __( 'Guateque (Boyacá)', 'woocommerce' ), 
'GC' => __( 'Guatica (Risaralda)', 'woocommerce' ), 
'GI' => __( 'Guayabal (Tolima)', 'woocommerce' ), 
'HA' => __( 'Hato nuevo (Guajira)', 'woocommerce' ),
'HO' => __( 'Hobo (Huila)', 'woocommerce' ), 
'HD' => __( 'Honda (Tolima)', 'woocommerce' ),
'IB' => __( 'Ibagué (Tolima)', 'woocommerce' ), 
'IP' => __( 'Ipiales (Nariño)', 'woocommerce' ), 
'IR' => __( 'Irra (Risaralda)', 'woocommerce' ), 
'JU' => __( 'Juan de Acosta (Atlántico)', 'woocommerce' ),
'LA' => __( 'La apartada (Córdoba)', 'woocommerce' ), 
'LD' => __( 'La Dorada (Caldas)', 'woocommerce' ),
'LE' => __( 'La esperanza (Santander)', 'woocommerce' ),
'LJ' => __( 'La jagua de ibirico (Cesar)', 'woocommerce' ), 
'LG' => __( 'La jagua del Pilar (Guajira)', 'woocommerce' ), 
'LL' => __( 'La Loma César', 'woocommerce' ),
'LP' => __( 'La paila (Valle)', 'woocommerce' ),
'LZ' => __( 'La paz (Cesar)', 'woocommerce' ),
'LT' => __( 'La plata (Huila)', 'woocommerce' ), 
'LS' => __( 'La Sierra (Tolima)', 'woocommerce' ), 
'LU' => __( 'La Unión(Antioquia)', 'woocommerce' ),
'LN' => __( 'La unión (Valle)', 'woocommerce' ), 
'LR' => __( 'La Uribe (Valle)', 'woocommerce' ), 
'LV' => __( 'La victoria (Valle)', 'woocommerce' ), 
'LY' => __( 'La Ye (Córdoba)', 'woocommerce' ), 
'LS' => __( 'Las Lajas (Nariño)', 'woocommerce' ), 
'LI' => __( 'lerida (Tolima)', 'woocommerce' ), 
'LB' => __( 'Líbano (Tolima)', 'woocommerce' ),
'LO' => __( 'Loboguerrero (Valle)', 'woocommerce' ),
'LC' => __( 'Lorica (Córdoba)', 'woocommerce' ), 
'LM' => __( 'Los palmitos sucre', 'woocommerce' ), 
'LX' => __( 'Luruaco atlantico', 'woocommerce' ),
'MA' => __( 'Magangué Bolívar', 'woocommerce' ), 
'MC' => __( 'Maicao (Guajira) ', 'woocommerce' ),
'MG' => __( 'Málaga Santander', 'woocommerce' ), 
'MI' => __( 'Manizales caldas ', 'woocommerce' ),
'MR' => __( 'Mariquita (Tolima) ', 'woocommerce' ),
'ME' => __( 'Medellín(Antioquia)', 'woocommerce' ),
'ML' => __( 'Melgar (Tolima) ', 'woocommerce' ),
'MD' => __( 'Miranda (Cauca)', 'woocommerce' ), 
'MO' => __( 'Momil (Córdoba)', 'woocommerce' ), 
'MQ' => __( 'Moniquira boyaca ', 'woocommerce' ),
'MB' => __( 'Montelíbano (Córdoba)', 'woocommerce' ), 
'MT' => __( 'Montería (Córdoba)', 'woocommerce' ), 
'NA' => __( 'Natagaima (Tolima) ', 'woocommerce' ),
'NE' => __( 'Neiva (Huila)', 'woocommerce' ), 
'NO' => __( 'Nobsa boyaca ', 'woocommerce' ),
'OB' => __( 'Obando (Valle) ', 'woocommerce' ),
'OC' => __( 'Ocaña norte de Santander', 'woocommerce' ), 
'OI' => __( 'Oiba Santander ', 'woocommerce' ),
'OR' => __( 'Ortega (Tolima)', 'woocommerce' ), 
'OT' => __( 'Ortigal (Cauca)', 'woocommerce' ),
'PA' => __( 'Pachaquiaro (Meta) ', 'woocommerce' ),
'PB' => __( 'Paipa (Boyacá)', 'woocommerce' ), 
'PC' => __( 'Palmar de Varela Atlántico ', 'woocommerce' ),
'PD' => __( 'Pamplona Norte Santander', 'woocommerce' ), 
'PE' => __( 'Pasto (Nariño) ', 'woocommerce' ),
'PF' => __( 'Pereira (Risaralda) ', 'woocommerce' ),
'PG' => __( 'Pescadero Santander', 'woocommerce' ), 
'PH' => __( 'Piendamo (Cauca)', 'woocommerce' ), 
'PI' => __( 'Pilcuan (Nariño)', 'woocommerce' ), 
'PJ' => __( 'Pinchote Santander ', 'woocommerce' ),
'PK' => __( 'Pitalito (Huila) ', 'woocommerce' ),
'PL' => __( 'Planeta rica (Córdoba)', 'woocommerce' ), 
'PN' => __( 'Plato (Magdalena) ', 'woocommerce' ),
'PO' => __( 'Ponedera atlantico ', 'woocommerce' ),
'PP' => __( 'Popayán (Cauca)', 'woocommerce' ), 
'PQ' => __( 'Pradera (Valle)', 'woocommerce' ), 
'PR' => __( 'Prado (Tolima)', 'woocommerce' ), 
'PS' => __( 'Presidente (Valle)', 'woocommerce' ), 
'PT' => __( 'Pueblo nuevo (Córdoba)', 'woocommerce' ), 
'PU' => __( 'Pueblo Nuevo (Magdalena) ', 'woocommerce' ),
'PV' => __( 'Puente (Boyacá) (Boyacá)', 'woocommerce' ),
'PW' => __( 'Puente nacional Santander', 'woocommerce' ), 
'PX1' => __( 'Puerto (Boyacá) ', 'woocommerce' ),
'PX2' => __( 'Puerto giraldo Atlántico', 'woocommerce' ), 
'PX3' => __( 'Puerto salgar (Cundinamarca)', 'woocommerce' ), 
'PX4' => __( 'Puerto Tejada (Cauca) ', 'woocommerce' ),
'PY' => __( 'Purificación (Tolima) ', 'woocommerce' ),
'PZ' => __( 'Purísima (Córdoba)', 'woocommerce' ), 
'QU' => __( 'Quinchía (Risaralda)', 'woocommerce' ), 
'RA' => __( 'REPELON Atlántico', 'woocommerce' ), 
'RE' => __( 'RESTREPO (Meta)', 'woocommerce' ),
'RD' => __( 'Río de oro César', 'woocommerce' ),
'RF' => __( 'Río Frío (Magdalena)', 'woocommerce' ), 
'RH' => __( 'Riohacha (Guajira)', 'woocommerce' ), 
'RN' => __( 'Rionegro Santander ', 'woocommerce' ),
'RS' => __( 'Río sucio caldas ', 'woocommerce' ),
'RI' => __( 'Risaralda caldas', 'woocommerce' ), 
'RV' => __( 'Rivera (Huila) ', 'woocommerce' ),
'RO' => __( 'Roldanillo (Valle)', 'woocommerce' ), 
'SA1' => __( 'Sabanagrande atlantico', 'woocommerce' ), 
'SA2' => __( 'Sabana chica (Boyacá) ', 'woocommerce' ),
'SA3' => __( 'Sahagun (Córdoba) ', 'woocommerce' ),
'SA4' => __( 'Saldaña (Tolima) ', 'woocommerce' ),
'SN0' => __( 'San Agustín (Huila)', 'woocommerce' ), 
'SN1' => __( 'San Alberto César ', 'woocommerce' ),
'SN2' => __( 'San antero (Córdoba) ', 'woocommerce' ),
'SN3' => __( 'San Clemente (Risaralda) ', 'woocommerce' ),
'SN4' => __( 'San Diego (Cesar) ', 'woocommerce' ),
'SN5' => __( 'San Felipe (Tolima) ', 'woocommerce' ),
'SN6' => __( 'San Juan (Nariño)', 'woocommerce' ),
'SN7' => __( 'San Juan del César (Guajira) ', 'woocommerce' ),
'SN8' => __( 'San Martín César ', 'woocommerce' ),
'SN9' => __( 'San Pedro sucre ', 'woocommerce' ),
'SNP' => __( 'San Pedro (Valle) ', 'woocommerce' ),
'SNY' => __( 'San Pelayo (Córdoba) ', 'woocommerce' ),
'SNM' => __( 'Santa Marta (Magdalena) ', 'woocommerce' ),
'SNR' => __( 'Santa Rosa de viterbo (Boyacá) (Boyacá)', 'woocommerce' ), 
'ST' => __( 'Santana (Boyacá) ', 'woocommerce' ),
'SD' => __( 'Santander de quilichao (Cauca) ', 'woocommerce' ),
'SC' => __( 'Santo Tomás Atlántico ', 'woocommerce' ),
'SE' => __( 'Sevilla (Valle) ', 'woocommerce' ),
'SI' => __( 'Sincelejo sucre ', 'woocommerce' ),
'SO' => __( 'Soata (Boyacá)', 'woocommerce' ), 
'SR' => __( 'Socorro Santander ', 'woocommerce' ),
'SG' => __( 'Sogamoso boyaca ', 'woocommerce' ),
'SQ' => __( 'Sonson(Antioquia)', 'woocommerce' ),
'SY' => __( 'Soraca boyaca ', 'woocommerce' ),
'SU' => __( 'Supía caldas', 'woocommerce' ), 
'SH' => __( 'Sutamarchán (Boyacá) ', 'woocommerce' ),
'TA' => __( 'Taganga (Magdalena)', 'woocommerce' ), 
'TB' => __( 'Tangua (Nariño) ', 'woocommerce' ),
'TC' => __( 'Tenza boyaca', 'woocommerce' ), 
'TD' => __( 'Tibasosa boyaca ', 'woocommerce' ),
'TE' => __( 'Tierra negra (Boyacá) ', 'woocommerce' ),
'TF' => __( 'Timana (Huila) ', 'woocommerce' ),
'TG' => __( 'Tolemaida (Tolima) ', 'woocommerce' ),
'TH' => __( 'Tolu sucre ', 'woocommerce' ),
'TI' => __( 'Toluviejo sucre', 'woocommerce' ),
'TJ' => __( 'Toro (Valle) ', 'woocommerce' ),
'TK' => __( 'Tubara atlantico ', 'woocommerce' ),
'TL' => __( 'Tuchin (Córdoba) ', 'woocommerce' ),
'TM' => __( 'Tucurinca (Magdalena)', 'woocommerce' ), 
'TN' => __( 'Tuluá (Valle) ', 'woocommerce' ),
'TO' => __( 'Tumaco (Nariño) ', 'woocommerce' ),
'TP' => __( 'Tunia (Cauca) ', 'woocommerce' ),
'TR' => __( 'Tunja (Boyacá)', 'woocommerce' ), 
'TS' => __( 'Tuquerres (Nariño)', 'woocommerce' ),
'TT' => __( 'Turbo (Antioquia)', 'woocommerce' ),
'TU' => __( 'Tuta (Boyacá)', 'woocommerce' ),
'UB' => __( 'Ubate (Cundinamarca)', 'woocommerce' ),
'UR' => __( 'Urumita (Guajira)', 'woocommerce' ),
'VA' => __( 'Valledupar (Cesar)', 'woocommerce' ),
'VE' => __( 'Velez (Santander)', 'woocommerce' ),
'VN' => __( 'Venadillo (Tolima) ', 'woocommerce' ),
'VT' => __( 'Ventaquemada boyaca ', 'woocommerce' ),
'VI' => __( 'Villanueva (Guajira)', 'woocommerce' ), 
'VP' => __( 'Villapinzon (Cundinamarca)', 'woocommerce' ),
'VR' => __( 'Villatrica (Cauca)', 'woocommerce' ),
'VC' => __( 'Villavicencio (Meta)', 'woocommerce' ),
'VL' => __( 'Villeta (Cundinamarca)', 'woocommerce' ),
'YO' => __( 'Yopal (Casanare)', 'woocommerce' ),
'ZA' => __( 'Zapatoca (Santander)', 'woocommerce' ),
'ZZ' => __( 'Zarzal (Valle)', 'woocommerce' )
);

  return $states;
}

?>