<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
global $verito_Options;
get_header('shop');

$plugin_url = plugins_url();

?>





  <?php

 do_action('woocommerce_before_main_content'); 

/**
 * woocommerce_before_main_content hook
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 */

?> 
<div class="main-container col2-left-layout bounceInUp animated">
    <div class="container">
      <div class="row">

        <div class="col-sm-9 col-sm-push-3 pro-grid">

         <?php do_action('woocommerce_archive_description'); ?>
         
         <div class="col-main pro-grid">   

      <?php if (have_posts()) : ?>
      
              <?php if(apply_filters('woocommerce_show_page_title', true)) : ?>
                   <div class="grid-heading">
                    <h2 class="page-heading"> <span class="page-heading-title">
                      <?php esc_html(woocommerce_page_title()); ?>
                    </span> <div class="display-product-option">
                    <div class="toolbar">
                    <?php
                    /**
                     * woocommerce_before_shop_loop hook
                     *
                     * @hooked woocommerce_result_count - 20
                     * @hooked woocommerce_catalog_ordering - 30
                     */
                    do_action('woocommerce_before_shop_loop');
                    ?>                   
                  </div>
                </div></h2>
                </div>
                    <?php endif; ?>

                <div class="category-products">
                    <?php woocommerce_product_loop_start(); ?>
                    <?php woocommerce_product_subcategories(); ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php wc_get_template_part('content', 'product'); ?>
                    <?php endwhile; // end of the loop. ?>
                    <?php woocommerce_product_loop_end(); ?>
                     </div>            
                     <div class="after-loop">
                      <?php
                    do_action('woocommerce_after_shop_loop');
                    ?>
                    </div>  
                <?php 
                elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) :         wc_get_template('loop/no-products-found.php');
                endif; ?>
                             
               

             </div>      
            </div>

    
           <div class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
       
           
         
                <?php
                /**
                 * woocommerce_sidebar hook
                 *
                 * @hooked woocommerce_get_sidebar - 10
                 */
                do_action('woocommerce_sidebar');
                ?>
               
            </div>
        
        </div>
    </div>
</div>
<?php
/**
 * woocommerce_after_main_content hook
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action('woocommerce_after_main_content');
?>

<?php get_footer('shop'); ?>
