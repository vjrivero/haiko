<?php
  if ( ! function_exists ( 'verito_top_navigation' ) ) {
function verito_top_navigation() {
global $verito_Options;
   
    $html = '';

    if (isset($verito_Options['login_button_pos']) && $verito_Options['login_button_pos'] == 'toplinks') {

        if (is_user_logged_in()) {
            $logout_link = '';
          if ( class_exists( 'WooCommerce' ) ) {
                $logout_link = wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) );
            } else {
                $logout_link = wp_logout_url( get_home_url() );
            }            $html .= '<li class="menu-item"><span><a href="' . esc_url($logout_link) . '"><i class="icon-lock-open"></i>' . esc_html__('Logout', 'verito') . '</a></span></li>';
            $myaccount_link = wc_get_page_permalink( 'myaccount' );
            $html .= '<li class="menu-item"><span><a href="' . esc_url($myaccount_link) . '"><i class="icon-lock-open"></i>' . esc_html__('My Account', 'verito') . '</a></span></li>';
        } else {
            $login_link = $register_link = '';
            if ( class_exists( 'WooCommerce' ) ) {
                $login_link = wc_get_page_permalink( 'myaccount' );
                if (get_option('woocommerce_enable_myaccount_registration') === 'yes') {
                    $register_link = wc_get_page_permalink( 'myaccount' );
                }
            } else {
                $login_link = wp_login_url( get_home_url() );
                $active_signup = get_site_option( 'registration', 'none' );
                $active_signup = apply_filters( 'wpmu_active_signup', $active_signup );
                if ($active_signup != 'none')
                    $register_link = wp_registration_url( get_home_url() );
            }
            $html .= '<li class="menu-item"><a href="' . esc_url($login_link) . '"> <i class="icon-lock"></i>' . esc_html__('Login', 'verito') . '</a></li>';
            if ($register_link) {
                $html .= '<li class="menu-item"><a href="' . esc_url($register_link) . '?register"><i class="icon-pencil"></i>' . esc_html__('Register', 'verito') . '</a></li>';
            }
        }
    }
    if(isset($verito_Options['show_menu_arrow']) && $verito_Options['show_menu_arrow'])
   {
    $mcls=' show-arrow';
   }
   else
   {
    $mcls='';
   }
    ob_start();
    if ( has_nav_menu( 'toplinks' ) ) :
    
        wp_nav_menu(array(
            'theme_location' => 'toplinks',
            'container' => '',
            'menu_class' => 'top-links1 mega-menu1' .$mcls,
            'before' => '',
            'after' => '',          
            'link_before' => '',
            'link_after' => '',
            'fallback_cb' => false,
            'walker' => new Verito_top_navwalker
        ));
    endif;

    $output = str_replace('&nbsp;', '', ob_get_clean());

    if ($output && $html) { 
        $output = preg_replace('/<\/ul>$/', $html . '</ul>', $output, 1);
    }

    return $output;
}
}

if ( ! function_exists ( 'verito_middle_navigation' ) ) {
function verito_middle_navigation() {
global $verito_Options;
   
    $html = '';

    if (isset($verito_Options['login_button_pos']) && $verito_Options['login_button_pos'] == 'middlelinks') {

        if (is_user_logged_in()) {
            $logout_link = '';
          if ( class_exists( 'WooCommerce' ) ) {
                $logout_link = wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) );
            } else {
                $logout_link = wp_logout_url( get_home_url() );
            }            $html .= '<li class="menu-item"><span><a href="' . esc_url($logout_link) . '">' . esc_html__('Logout', 'verito') . '</a></span></li>';
        } else {
            $login_link = $register_link = '';
            if ( class_exists( 'WooCommerce' ) ) {
                $login_link = wc_get_page_permalink( 'myaccount' );
                if (get_option('woocommerce_enable_myaccount_registration') === 'yes') {
                    $register_link = wc_get_page_permalink( 'myaccount' );
                }
            } else {
                $login_link = wp_login_url( get_home_url() );
                $active_signup = get_site_option( 'registration', 'none' );
                $active_signup = apply_filters( 'wpmu_active_signup', $active_signup );
                if ($active_signup != 'none')
                    $register_link = wp_registration_url( get_home_url() );
            }
            $html .= '<li class="menu-item"><a href="' . esc_url($login_link) . '"> <span>' . esc_html__('Login', 'verito') . '</span></a></li>';
            if ($register_link) {
                $html .= '<li class="menu-item"><a href="' . esc_url($register_link) . '"><span>' . esc_html__('Register', 'verito') . '</span></a></li>';
            }
        }
    }
    if(isset($verito_Options['show_menu_arrow']) && $verito_Options['show_menu_arrow'])
   {
    $mcls=' show-arrow';
   }
   else
   {
    $mcls='';
   }
    ob_start();
    if ( has_nav_menu( 'middlelinks' ) ) :
    
        wp_nav_menu(array(
            'theme_location' => 'middlelinks',
            'container' => '',
            'menu_class' => 'middle-links1 mega-menu1' .$mcls,
            'before' => '',
            'after' => '',          
            'link_before' => '',
            'link_after' => '',
            'fallback_cb' => false,
            'walker' => new Verito_middle_navwalker
        ));
    endif;

    $output = str_replace('&nbsp;', '', ob_get_clean());

    if ($output && $html) { 
        $output = preg_replace('/<\/ul>$/', $html . '</ul>', $output, 1);
    }

    return $output;
}
}


  if ( ! function_exists ( 'verito_mobile_top_navigation' ) ) {
function verito_mobile_top_navigation() {
global $verito_Options;
   
    $html = '';

    if (isset($verito_Options['login_button_pos']) && $verito_Options['login_button_pos'] == 'toplinks') {

        if (is_user_logged_in()) {
            $logout_link = '';
            $myaccount_link = wc_get_page_permalink( 'myaccount' );
            $html .= '<li class="menu-item"><a href="' . esc_url($myaccount_link) . '"><span>' . esc_html__('My Account', 'verito') . '</span></a></li>';
          if ( class_exists( 'WooCommerce' ) ) {
                $logout_link = wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) );
            } else {
                $logout_link = wp_logout_url( get_home_url() );
            }            $html .= '<li class="menu-item"><a href="' . esc_url($logout_link) . '"><span>' . esc_html__('Logout', 'verito') . '</span></a></li>';
        
        } else {
            $login_link = $register_link = '';
            if ( class_exists( 'WooCommerce' ) ) {
                $login_link = wc_get_page_permalink( 'myaccount' );
                if (get_option('woocommerce_enable_myaccount_registration') === 'yes') {
                    $register_link = wc_get_page_permalink( 'myaccount' );
                }
            } else {
                $login_link = wp_login_url( get_home_url() );
                $active_signup = get_site_option( 'registration', 'none' );
                 $active_signup = apply_filters( 'wpmu_active_signup', $active_signup );
                if ($active_signup != 'none')
                    $register_link = wp_registration_url( get_home_url() );
            }
            $html .= '<li class="menu-item"><a href="' . esc_url($login_link) . '"><span>' . esc_html__('Login', 'verito') . '</span></a></li>';
            if ($register_link) {
                $html .= '<li class="menu-item"><a href="' . esc_url($register_link) . '?register"><span>' . esc_html__('Register', 'verito') . '</span></a></li>';
            }
        }
    }
   if(isset($verito_Options['show_menu_arrow']) && $verito_Options['show_menu_arrow'])
   {
    $mcls=' show-arrow';
   }
   else
   {
    $mcls='';
   }
    ob_start();
    if ( has_nav_menu( 'toplinks' ) ) :
        wp_nav_menu(array(
            'theme_location' => 'toplinks',
            'container' => '',
            'menu_class' => 'top-links1 accordion-menu' . $mcls,
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'fallback_cb' => false,
            'walker' => new Verito_mobile_navwalker
        ));
    endif;

    $output = str_replace('&nbsp;', '', ob_get_clean());

    if ($output && $html) {
        $output = preg_replace('/<\/ul>$/', $html . '</ul>', $output, 1);
    }

    return $output;
}
}


  if ( ! function_exists ( 'verito_main_menu' ) ) {
function verito_main_menu() {
    global $verito_Options;
   
    $html = '';

    if (isset($verito_Options['login_button_pos']) && $verito_Options['login_button_pos'] == 'main_menu') {

        if (is_user_logged_in()) {
            $logout_link = '';
            if ( class_exists( 'WooCommerce' ) ) {
                $logout_link = wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) );
            } else {
                $logout_link = wp_logout_url( get_home_url() );
            }

           
                $html .= '<li class="menu-item "><a href="' . esc_url($logout_link) . '"><span>' . esc_html__('Logout', 'verito') . '</span></a></li>';         
        } 
        else {
            $login_link = $register_link = '';
            if (class_exists( 'WooCommerce' ) ) {
                $login_link = wc_get_page_permalink( 'myaccount' );
                if (get_option('woocommerce_enable_myaccount_registration') === 'yes') {
                    $register_link = wc_get_page_permalink( 'myaccount' );
                }
            } else {
                $login_link = wp_login_url( get_home_url() );
                $active_signup = get_site_option( 'registration', 'none' );
                $active_signup = apply_filters( 'wpmu_active_signup', $active_signup );
         
                if ($active_signup != 'none')
                     $register_link = wp_registration_url( get_home_url() );
            }
           
                if ($register_link) {
                    $html .= '<li class="menu-item"><a href="' . esc_url($register_link) . '"><span>' . esc_html__('Register', 'verito') . '</span></a></li>';
                }
                $html .= '<li class="menu-item"><a href="' . esc_url($login_link) . '"><span>' . esc_html__('Login', 'verito') . '</span></a></li>';
           
        }
    }
    if(isset($verito_Options['show_menu_arrow']) && $verito_Options['show_menu_arrow'])
   {
    $mcls=' show-arrow';
   }
   else
   {
    $mcls='';
   }
    ob_start();
    if ( has_nav_menu('main_menu') ) :     
        $args = array(
        'container' => '',
        'menu_class' => 'main-menu mega-menu' . $mcls,
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'fallback_cb' => false,
            'walker' => new Verito_mainmenu_navwalker
        );
       
            $args['theme_location'] = 'main_menu';
        
        wp_nav_menu($args);
    endif;

    $output = str_replace('&nbsp;', '', ob_get_clean());

    if ($output && $html) {

        $output = preg_replace('/<\/ul>$/', $html . '</ul>', $output, 1);
    }

    return $output;
}
}


  if ( ! function_exists ( 'verito_mobile_menu' ) ) {
function verito_mobile_menu() {
    global $verito_Options;

    $html = '';
    if (isset($verito_Options['login_button_pos']) && $verito_Options['login_button_pos'] == 'main_menu') {
        if (is_user_logged_in()) {
            $logout_link = '';
            if ( class_exists( 'WooCommerce' ) ) {
              $logout_link = wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) );
            } else {
                $logout_link = wp_logout_url( get_home_url() );
            }
            $html .= '<li class="menu-item"><a href="' . esc_url($logout_link) . '"><span>' . esc_html__('Logout', 'verito') . '</span></a></li>';
        } else {
            $login_link = $register_link = '';
            if ( class_exists( 'WooCommerce' ) ) {
                $login_link = wc_get_page_permalink( 'myaccount' );
                if (get_option('woocommerce_enable_myaccount_registration') === 'yes') {
                    $register_link = wc_get_page_permalink( 'myaccount' );
                }
            } else {
                $login_link = wp_login_url( get_home_url() );
                $active_signup = get_site_option( 'registration', 'none' );
                $active_signup = apply_filters( 'wpmu_active_signup', $active_signup );
                if ($active_signup != 'none')
                    $register_link = wp_registration_url( get_home_url() );
            }
            $html .= '<li class="menu-item"><a href="' . esc_url($login_link) . '"><span>' . esc_html__('Login', 'verito') . '</span></a></li>';
            if ($register_link ) {
                $html .= '<li class="menu-item"><a href="' . esc_url($register_link) . '"><span>' . esc_html__('Register', 'verito') . '</span></a></li>';
            }
        }
    }

   
    ob_start();
    if ( has_nav_menu( 'main_menu' ) ) :
      
        $args = array(
            'container' => '',
            'menu_class' => 'mobile-menu accordion-menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'fallback_cb' => false,
            'walker' => new Verito_mobile_navwalker
        );
      
            $args['theme_location'] = 'main_menu';
        
        wp_nav_menu($args);
    endif;

    $output = str_replace('&nbsp;', '', ob_get_clean());


    if ($output && $html) {
        $output = preg_replace('/<\/ul>$/', $html . '</ul>', $output, 1);
    }

    return $output;
}
}


?>