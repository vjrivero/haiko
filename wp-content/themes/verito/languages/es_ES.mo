��    b      ,      <      <  '   =     e     y     �     �     �     �     �     �     �     �     �  
   �       
             !     )     ;     O     [     j  	   m     w  &        �     �     �     �     �     �     �     �     �     �          
            
        )  	   1     ;     G     K     X     e  +   w     �     �     �     �     �     �     �     �  &   �     %	     .	  	   ;	     E	  
   I	  	   T	  	   ^	  	   h	     r	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
  '   
     E
  '   \
     �
     �
     �
     �
  &   �
     �
     �
     �
  	                   )     -     5     ;  	   B     L  [  U  ,   �     �     �               *     =     S     g     v     �     �     �     �     �     �     �     �     �     �          !     $     0  ,   8  	   e     o     �     �  	   �     �     �     �     �     �     �     �  
   �     �  	   �  
                  /     5     F     X  8   t     �     �     �  	   �     �     �     �       /   !  	   Q     [  	   i     s     y  	   �  	   �  	   �     �     �     �     �     �     �          (     /  !   6     X     m  ,   {  !   �  /   �     �                0  '   9     a     v  	   �     �     �     �     �     �     �     �     �     �   %1$d = first, %2$d = last, %3$d = total 404! Page Not Found Add to Cart Add to Compare Add to WishList Add to Wishlist All Categories All Products Apply Coupon Back To Home Buy product By Categories Checkout Clear Cart Comments Compare Continue Shopping Continue reading... Coupon code Discount Codes EN Edit item English Enter your coupon code if you have one Featured Featured Product Featured Products Grid Home Latest List Log In Log Out Logged in as Login Logout MY CART Menu My Account My Cart My Search My Wishlist New New Products Next &raquo; No products found Oops! The Page you requested was not found! Out of stock Page Pages Pages: Pages:&nbsp Pages:&nbsp; Proceed to Checkout Product Banner Product successfully added to wishlist Products Published by QUANTITY: Qty Quick View READ MORE Read More Read more Recently added item(s) Recently added items Recommended Register Related Related Products Remove This Item Sale Search Search entire store here... Select options Shop now Showing %1$d&ndash;%2$d of %3$d results Showing all %d results Sorry, What you are looking isn't here. Sorry, nothing in cart. Sort By Special Price Subtotal The product is already in the wishlist Type your search Update Cart VIEW ALL View Cart View products Wishlist Yes average items out of posted by see more Project-Id-Version: verito 1.1
Report-Msgid-Bugs-To: http://wordpress.magikcommerce.com/verito
POT-Creation-Date: 2016-04-11 15:11:26+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-02-17 00:18-0400
Last-Translator: 
Language-Team: 
Language: es_ES
X-Generator: Poedit 1.8.11
 %1$d = primero, %2$d = último, %3$d = total ¡404! Página no encontrada Agregar al Carrito Comparar Agregar a deseados Agregar a deseados Todas las Categorías Todos los Productos Aplicar cupón Ir al inicio Comprar producto Por Categorías Revisar Vaciar Carrito Comentarios Comparar Continuar comprando Continuar leyendo... Código de cupón Códigos de descuento EB Editar item Inglés Introduzca el código de cupón si posee uno Destacado Productos Destacados Productos Destacados Cuadrícula Productos Reciente Lista Entrar Salir Autenticado como Entrar Salir MI CARRITO Menú Mi Cuenta Mi Carrito Mi Búsqueda My lista de deseos Nuevo Nuevos Productos Siguiente &raquo; Ningún producto encontrado ¡Ups! ¡La página que ha solicitado no fue encontrada! Existencia agotada Página Páginas Páginas: Páginas:&nbsp Páginas:&nbsp; Proceder a revisar Banner de productos Producto agregado satisfactoriamente a deseados Productos Publicado por CANTIDAD: Cant. Vista rápida LEER MÁS Leer Más Leer más Recientemente agregados Recientemente agregados Recomendado Registrarse Relacionados Productos Relacionados Eliminar este producto Oferta Buscar Buscar en toda la tienda aquí... Seleccionar opciones Comprar ahora Mostrando %1$d&ndash;%2$d de %3$d resultados Mostrando todos los %d resultados DIsculpe, lo que está buscando no está aquí. Disculpe, carrito vacío. Ordenar por Precio especial Subtotal El producto ya se encuentra en deseados Escribe tu búsqueda Actualizar Carrito VER TODOS Ver Carrito Ver productos Lista de deseos Sí promedio items fuera de publicado por ver más 