<?php 

$Verito = new Verito();?>

<?php if ((!is_front_page() && !is_home()) || !is_front_page()) { ?>
<?php verito_header_service();?> 
<?php } ?>

<?php verito_footer_brand_logo();?>
<footer>
    <div class="footer-inner">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-xs-12 col-lg-8">
            
    
          <?php if (is_active_sidebar('footer-sidebar-1')) : ?>
                           
            <div class="footer-column pull-left">
               <?php dynamic_sidebar('footer-sidebar-1'); ?>
            </div>
          
          <?php endif; ?>
           
           <?php if (is_active_sidebar('footer-sidebar-2')) : ?>
            
            <div class="footer-column pull-left">
               <?php dynamic_sidebar('footer-sidebar-2'); ?>
            </div>

            <?php endif; ?>
           
           <?php if (is_active_sidebar('footer-sidebar-3')) : ?>

            <div class="footer-column pull-left">
               <?php dynamic_sidebar('footer-sidebar-3'); ?>
            </div>
            
            <?php endif; ?>

          </div>

         <div class="col-xs-12 col-lg-4">
            <div class="footer-column-last">
              <?php verito_footer_signupform();?>
              <?php verito_social_media_links(); ?>
             <?php if (is_active_sidebar('footer-sidebar-4')) : ?>
                <?php dynamic_sidebar('footer-sidebar-4'); ?>
            <?php endif; ?> 
            </div>

        </div>
      </div>
    </div>
   </div>

    <div class="footer-middle">
      <?php verito_footer_middle();?>
    </div>
    

   <?php verito_footer_text(); ?> 

  </footer>

</div>
<?php verito_backtotop();?>
    
<div class="menu-overlay"></div>
<?php // navigation panel
get_template_part('menu_panel');
 ?>
    <?php wp_footer(); ?>
</body></html>
