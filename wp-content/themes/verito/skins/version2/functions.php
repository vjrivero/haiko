<?php
  if ( ! function_exists ( 'verito_currency_language' ) ) {
function verito_currency_language()
{ 
     global $verito_Options;
      
      
        if(isset($verito_Options['enable_header_language']) && ($verito_Options['enable_header_language']!=0))
        { ?>
          <div class="dropdown block-language-wrapper"> 
            <a role="button" data-toggle="dropdown" data-target="#" class="block-language dropdown-toggle" href="#"> 
              <img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/english.png" alt="<?php esc_attr_e('English', 'verito');?>">  
              <?php esc_attr_e('English ', 'verito');?><span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/english.png" alt="<?php esc_attr_e('English', 'verito');?>">    <?php esc_attr_e('English', 'verito');?></a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/francais.png" alt="<?php esc_attr_e('French', 'verito');?>"> <?php esc_attr_e('French', 'verito');?> </a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/german.png" alt="<?php esc_attr_e('German', 'verito');?>">   <?php esc_attr_e('German', 'verito');?></a></li>
            </ul>
          </div>
        <?php  
        } 

        if(isset($verito_Options['enable_header_currency']) && ($verito_Options['enable_header_currency']!=0))
        { ?>
          <div class="dropdown block-currency-wrapper"> 
            <a role="button" data-toggle="dropdown" data-target="#" class="block-currency dropdown-toggle" href="#">  
              <?php esc_attr_e('USD', 'verito');?> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li role="presentation">
                <a role="menuitem" tabindex="-1" href="#">
                <?php esc_attr_e('$ - Dollar', 'verito');?>
                </a>
              </li>
              <li role="presentation">
                <a role="menuitem" tabindex="-1" href="#">
                <?php esc_attr_e('&pound; - Pound', 'verito');?>
                </a>
              </li>
              <li role="presentation">
                <a role="menuitem" tabindex="-1" href="#">
                <?php esc_attr_e('&euro; - Euro', 'verito');?>
                </a>
              </li>
            </ul>
          </div>
        <?php  
        } 
        
       
}
}

  if ( ! function_exists ( 'verito_msg' ) ) {
function verito_msg()
{ 
     global $verito_Options;
 
           if (is_user_logged_in()) {
            global $current_user;
            
            if(isset($verito_Options['welcome_msg']) && ($verito_Options['welcome_msg']!='')) {
            echo esc_attr_e('Logged in as', 'verito'). '   <b>'. esc_attr($current_user->display_name) .'</b>';
          }
          }
          else{
            if(isset($verito_Options['welcome_msg']) && ($verito_Options['welcome_msg']!='')){
            echo htmlspecialchars_decode($verito_Options['welcome_msg']);
            }
          } 
}
}

  if ( ! function_exists ( 'verito_logo_image' ) ) {
function verito_logo_image()
{ 
     global $verito_Options;
    
        $logoUrl = get_template_directory_uri() . '/images/logo.png';
        
        if (isset($verito_Options['header_use_imagelogo']) && $verito_Options['header_use_imagelogo'] === 0) {           ?>
        <a class="logo logotext" title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> ">
        <?php bloginfo('name'); ?>
        </a>
        <?php
        } else if (isset($verito_Options['header_logo']['url']) && !empty($verito_Options['header_logo']['url'])) { 
                  $logoUrl = $verito_Options['header_logo']['url'];
                  ?>
        <a class="logo" title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> "> <img
                      alt="<?php bloginfo('name'); ?>" src="<?php echo esc_url($logoUrl); ?>"
                      height="<?php echo !empty($verito_Options['header_logo_height']) ? esc_html($verito_Options['header_logo_height']) : ''; ?>"
                      width="<?php echo !empty($verito_Options['header_logo_width']) ? esc_html($verito_Options['header_logo_width']) : ''; ?>"> </a>
        <?php
        } else { ?>
        <a class="logo" title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> "> 
          <img src="<?php echo esc_url($logoUrl) ;?>" alt="<?php bloginfo('name'); ?>"> </a>
        <?php }  

}
}

 if ( ! function_exists ( 'verito_mobile_search' ) ) {
function verito_mobile_search()
{ global $verito_Options;
  $Verito = new Verito();
    if (isset($verito_Options['header_remove_header_search']) && !$verito_Options['header_remove_header_search']) : 
        echo'<div class="mobile-search">';
         echo verito_search_form();
         echo'<div class="search-autocomplete" id="search_autocomplete1" style="display: none;"></div></div>';
         endif;
}
}


 if ( ! function_exists ( 'verito_search_form' ) ) {
 function verito_search_form()
  {  
    global $verito_Options;
  $Verito = new Verito();
  ?>
  <?php if (isset($verito_Options['header_remove_header_search']) && !$verito_Options['header_remove_header_search']) : ?>
<div class="search-box">
<form name="myform"  method="GET" action="<?php echo esc_url(home_url('/')); ?>">
  <input class="mgksearch" type="text" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e('Search entire store here...','verito');?>" maxlength="70" name="s">
         <?php if (class_exists('WooCommerce')) : ?>    
              <input type="hidden" value="product" name="post_type">
               <?php endif; ?>       
   <button class="search-btn-bg" type="submit"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
                   
  </form>
</div>

   <?php  endif; ?>
  <?php
  }
}




 if ( ! function_exists ( 'verito_home_page_banner' ) ) {
function verito_home_page_banner()
{
    global $verito_Options;
     
        ?>

<div id="magik-slideshow" class="magik-slideshow">
    <div class="container">
      <div class="row">

        <div class="col-md-9 col-sm-12">

          <?php  if(isset($verito_Options['enable_home_gallery']) && $verito_Options['enable_home_gallery']  && isset($verito_Options['home-page-slider']) && !empty($verito_Options['home-page-slider'])) { ?>
      
         <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container'>
            <div id='rev_slider_5' class='rev_slider fullwidthabanner'>
                    <ul>
                            <?php foreach ($verito_Options['home-page-slider'] as $slide) : ?>

                                 <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='<?php echo esc_url($slide['thumb']); ?>'>
                                       <img
                                        src="<?php echo esc_url($slide['image']); ?>" data-data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' alt="<?php echo esc_url($slide['image']); ?>"/>
                                         <?php echo htmlspecialchars_decode($slide['description']); ?>
                                          <a class="s-link" href="<?php echo !empty($slide['url']) ? esc_url($slide['url']) : '#' ?>"></a>
                                     
                                
                                
                                </li>
                           
                              <?php endforeach; ?> 
                    </ul>
            </div>
          </div>
      
        <?php } ?>

       </div>
        

      <div class="col-md-3 col-sm-12">
            
            <?php verito_home_page_rhs_banners(); ?>
          
      </div>
      
      </div>
    </div>
</div>

<?php 
   
}
}


 if ( ! function_exists ( 'verito_home_sub_banner' ) ) {
function verito_home_sub_banner()
{
  global $verito_Options;
   if (isset($verito_Options['enable_home_sub_banner']) && $verito_Options['enable_home_sub_banner']){
        ?>

   <div class="bottom-banner-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="banner-inner">
           

              <?php if (isset($verito_Options['home-sub-banner-text']) &&  !empty($verito_Options['home-sub-banner-text'])){?> 
                 <?php echo htmlspecialchars_decode($verito_Options['home-sub-banner-text']);?>
              <?php }  ?>
              
            </div>
            <a href="<?php echo !empty($verito_Options['home-sub-banner-url']) ? esc_url($verito_Options['home-sub-banner-url']) : '#' ?>" title="<?php echo esc_html($verito_Options['home-sub-banner_title']);?>">
              <img src="<?php echo esc_url($verito_Options['home-sub-banner']['url']); ?>" alt="<?php echo esc_html($verito_Options['home-sub-banner_title']);?>"></a>
            
        </div>
      </div>
    </div>
  </div>

<?php }  ?>
<?php 
} 
}


 if ( ! function_exists ( 'verito_footer_signupform' ) ) {
function verito_footer_signupform()
{
  global $verito_Options;
if (isset($verito_Options['enable_mailchimp_form']) && !empty($verito_Options['enable_mailchimp_form'])) {
 if( function_exists('mc4wp_show_form'))
  {
  ?> 
   <div class="newsletter-wrap">
        <?php
          mc4wp_show_form();
        ?>
           
    </div>
  <?php
    } 
  }  
}
}

 if ( ! function_exists ( 'verito_footer_middle' ) ) {
function verito_footer_middle()
{
  global $verito_Options;
 
   if (isset($verito_Options['enable_footer_middle']) && !empty($verito_Options['footer_middle']))
  {?>
      
  <?php echo htmlspecialchars_decode($verito_Options['footer_middle']);?>
            
   <?php  
  }  
}
}

 if ( ! function_exists ( 'verito_header_service' ) ) {
function verito_header_service()
{
  
global $verito_Options;

if (isset($verito_Options['header_show_info_banner']) && !empty($verito_Options['header_show_info_banner'])) :
                  ?>
   
    <div class="our-features-box top_header">
      <div class="container">
        <div class="row">

          <?php if (!empty($verito_Options['header_shipping_banner'])) : ?>
          <div class="col-lg-3 col-xs-12 col-sm-6">
            <div class="feature-box first"> <span class="fa fa-truck"></span>
              <div class="content">
                <h3><?php echo htmlspecialchars_decode($verito_Options['header_shipping_banner']); ?></h3>
              </div>
            </div>
          </div>
         <?php endif; ?>
         <?php if (!empty($verito_Options['header_customer_support_banner'])) : ?>
          <div class="col-lg-3 col-xs-12 col-sm-6">
            <div class="feature-box"> <span class="fa fa-headphones"></span>
              <div class="content">
                <h3><?php echo htmlspecialchars_decode($verito_Options['header_customer_support_banner']); ?></h3>
              </div>
            </div>
          </div>
         <?php endif; ?>
         <?php if (!empty($verito_Options['header_returnservice_banner'])) : ?>
          <div class="col-lg-3 col-xs-12 col-sm-6">
            <div class="feature-box"> <span class="fa fa-share"></span>
              <div class="content">
                <h3><?php echo htmlspecialchars_decode($verito_Options['header_returnservice_banner']); ?> </h3>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <?php if (!empty($verito_Options['header_moneyback_banner'])) : ?>
          <div class="col-lg-3 col-xs-12 col-sm-6">
            <div class="feature-box last"> <span class="fa fa-phone"></span>
              <div class="content">
                <h3><?php echo htmlspecialchars_decode($verito_Options['header_moneyback_banner']); ?></h3>
              </div>
            </div>
          </div>
        <?php endif; ?>

        </div>
      </div>
    </div>


    <?php
   
     endif; 
}
}


if ( ! function_exists ( 'verito_footer_service' ) ) {
function verito_footer_service()
{
  
global $verito_Options;

if (isset($verito_Options['header_show_info_banner']) && !empty($verito_Options['header_show_info_banner'])) :
                  ?>
   
    <div class="our-features-box">
      <div class="container">
        <div class="row">

          <?php if (!empty($verito_Options['header_shipping_banner'])) : ?>
          <div class="col-lg-3 col-xs-12 col-sm-6">
            <div class="feature-box first"> <span class="fa fa-truck"></span>
              <div class="content">
                <h3><?php echo htmlspecialchars_decode($verito_Options['header_shipping_banner']); ?></h3>
              </div>
            </div>
          </div>
         <?php endif; ?>
         <?php if (!empty($verito_Options['header_customer_support_banner'])) : ?>
          <div class="col-lg-3 col-xs-12 col-sm-6">
            <div class="feature-box"> <span class="fa fa-headphones"></span>
              <div class="content">
                <h3><?php echo htmlspecialchars_decode($verito_Options['header_customer_support_banner']); ?></h3>
              </div>
            </div>
          </div>
         <?php endif; ?>
         <?php if (!empty($verito_Options['header_returnservice_banner'])) : ?>
          <div class="col-lg-3 col-xs-12 col-sm-6">
            <div class="feature-box"> <span class="fa fa-share"></span>
              <div class="content">
                <h3><?php echo htmlspecialchars_decode($verito_Options['header_returnservice_banner']); ?> </h3>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <?php if (!empty($verito_Options['header_moneyback_banner'])) : ?>
          <div class="col-lg-3 col-xs-12 col-sm-6">
            <div class="feature-box last"> <span class="fa fa-phone"></span>
              <div class="content">
                <h3><?php echo htmlspecialchars_decode($verito_Options['header_moneyback_banner']); ?></h3>
              </div>
            </div>
          </div>
        <?php endif; ?>

        </div>
      </div>
    </div>


    <?php
   
     endif; 
}
}




 if ( ! function_exists ( 'verito_home_page_rhs_banners' ) ) {
function verito_home_page_rhs_banners()
{
 global $verito_Options;
    ?>
   <?php
  if (isset($verito_Options['enable_home_page_rhs_banners']) && $verito_Options['enable_home_page_rhs_banners']){
                  ?>

    <?php if (isset($verito_Options['home-banner1']) &&  !empty($verito_Options['home-banner1']['url'])){?>  


         <div class="home-banner">
            <?php if (isset($verito_Options['home-banner1-text']) &&  !empty($verito_Options['home-banner1-text'])){?>  
               <div class="banner-details">
                  <?php echo htmlspecialchars_decode($verito_Options['home-banner1-text']); ?>
               </div>
                 <?php }  ?>
            <a href="<?php echo !empty($verito_Options['home-banner1-url']) ? esc_url($verito_Options['home-banner1-url']) : '#' ?>" title="<?php echo esc_html($verito_Options['home-banner1-title']);?>">
            <img src="<?php echo esc_url($verito_Options['home-banner1']['url']); ?>" alt="<?php echo esc_html($verito_Options['home-banner1-title']);?>">

          </a>
        </div>
     
    <?php }  ?>

    <?php if (isset($verito_Options['home-banner2']) &&  !empty($verito_Options['home-banner2']['url'])){?> 

     <div class="home-rgt-banner">
         <div class="ad-img">
            <a href="<?php echo !empty($verito_Options['home-banner2-url']) ? esc_url($verito_Options['home-banner2-url']) : '#' ?>" title="<?php echo esc_html($verito_Options['home-banner2-title']);?>">
            
            <img src="<?php echo esc_url($verito_Options['home-banner2']['url']); ?>" alt="<?php echo esc_html($verito_Options['home-banner2-title']);?>">
            </a>
         </div>
         <div class="ad-content">
          <?php if (isset($verito_Options['home-banner2-text']) &&  !empty($verito_Options['home-banner2-text'])){?> 
           <?php echo htmlspecialchars_decode($verito_Options['home-banner2-text']);?>
              <?php }  ?>
         </div>
      </div>
      
      <?php }  ?>

     <?php if (isset($verito_Options['home-banner3']) &&  !empty($verito_Options['home-banner3']['url'])){?>

      <div class="home-rgt-banner green-bg">
        <div class="ad-content">
           <?php if (isset($verito_Options['home-banner3-text']) &&  !empty($verito_Options['home-banner3-text'])){?> 
          <?php echo htmlspecialchars_decode($verito_Options['home-banner3-text']);?>
           <?php }  ?>
       </div>
        <div class="ad-img">
          <a href="<?php echo !empty($verito_Options['home-banner3-url']) ? esc_url($verito_Options['home-banner3-url']) : '#' ?>" title="<?php echo esc_html($verito_Options['home-banner3-title']);?>">
            
           <img src="<?php echo esc_url($verito_Options['home-banner3']['url']); ?>" alt="<?php echo esc_html($verito_Options['home-banner3-title']);?>">
           </a>
        </div>
      </div>
      
      <?php }  ?>

                 

            <?php }  ?>


    <?php

}
}

 if ( ! function_exists ( 'verito_hotdeal_product' ) ) {
function verito_hotdeal_product()
{
   global $verito_Options;
if (isset($verito_Options['enable_home_hotdeal_products']) && !empty($verito_Options['enable_home_hotdeal_products'])) { 
  
  ?>

     <?php
      $args = array(
            'post_type'      => 'product',
            'posts_per_page' => 1,
            
            'meta_query'     => array(

              'relation' => 'AND',
                    array( // Simple products type
                        'key'           => 'hotdeal_on_homebanner',
                        'value'         => 'yes',
                        'compare'       => '=',
                       
                    ),
              
              array(
                    'relation' => 'OR',
                    array( // Simple products type
                        'key'           => '_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    ),
                  
                    array( // Variable products type
                        'key'           => '_min_variation_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    )
                    )
                
                )
        );
  
           $loop = new WP_Query( $args );
             if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post();
              verito_hotdeal_template();
            
            endwhile;
        } else {
           esc_attr_e('No products found','verito');
        }
        wp_reset_postdata();
    ?>

         
  <?php
}
}
}


 if ( ! function_exists ( 'verito_new_products' ) ) {
function verito_new_products()
{
   global $verito_Options;

if (isset($verito_Options['enable_home_new_products']) && !empty($verito_Options['enable_home_new_products']) && !empty($verito_Options['home_new_products_categories'])) {?>


  <div class="content-page">
    <div class="container">
      <div class="row">
        
    
    
    
        <!-- featured category fashion -->
        <div class="col-md-12">
          <div class="category-product">
            <div class="navbar nav-menu">
              <div class="navbar-collapse">
                <ul class="nav navbar-nav">
                  <li>
                    <div class="new_title">
                      <h2><?php esc_attr_e('New', 'verito'); ?> <em><?php esc_attr_e('Products', 'verito'); ?></em> </h2>
                    </div>
                  </li>
            
          
<?php
$catloop=1;


 foreach($verito_Options['home_new_products_categories'] as $category)
 {
  $term = get_term_by( 'id', $category, 'product_cat', 'ARRAY_A' );
  
  ?>
   <li class="<?php if($catloop==1){?> active <?php } ?>">
    <a href="#cat-<?php echo esc_html($category) ?>" data-toggle="tab"><?php echo esc_html($term['name']); ?>
    </a>
  </li>

  <?php 
  $catloop++;
  } ?>
    </ul>    
  </div>
</div>

  <!-- Tab panes -->
 <div class="product-bestseller">
   <div class="product-bestseller-content">
     <div class="product-bestseller-list">
       <div class="tab-container"> 
    <?php 
    $contentloop=1;
  foreach($verito_Options['home_new_products_categories'] as $catcontent)
 {
   $term = get_term_by( 'id', $catcontent, 'product_cat', 'ARRAY_A' );
?>
     <div class="tab-panel <?php if($contentloop==1){?> active <?php } ?>" id="cat-<?php echo esc_html($catcontent); ?>">
      <div class="category-products">
       <ul class="products-grid">

  <?php

 $args = array(
            'post_type'    => 'product',
            'post_status' => 'publish',
            'ignore_sticky_posts'    => 1,
            'posts_per_page' => $verito_Options['new_products_per_page'],
            
             'orderby' => 'date',
            'order' => 'DESC',
            'tax_query' => array(
                
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'term_id',
                    'terms' => $catcontent
                )
            ),

                
        );

                                $loop = new WP_Query( $args );
                             
                                if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();                  
                                verito_new_products_template();
                                endwhile;
                                } else {
                                esc_attr_e( 'No products found', 'verito' );
                                }

                               wp_reset_postdata();
                               $contentloop++;
                             
?>

            </ul>
        </div>   
      </div>
 <?php  } ?>

        </div>                  
      </div>
     </div>
   </div>

</div>
</div>
</div>
</div>
</div>


<?php 
}
}
}

 if ( ! function_exists ( 'verito_featured_products' ) ) {
function verito_featured_products()
{

global $verito_Options;
if (isset($verito_Options['enable_home_featured_products']) && !empty($verito_Options['enable_home_featured_products'])) {
        ?>

 <div class="featured-pro">
    <div class="container">
      <div class="slider-items-products">
        <div class="featured-block">
          <div class="home-block-inner">
            <div class="block-title">
                <h2><?php esc_attr_e('Featured ', 'verito'); ?><em><?php esc_attr_e('Product', 'verito'); ?></em></h2> 
              </div>
             
          </div>
          <div id="featured-slider" class="product-flexslider hidden-buttons">
            
            <div class="slider-items slider-width-col4 products-grid block-content">
                <?php
                $args = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'meta_key' => '_featured',
                    'meta_value' => 'yes',                   
                    'posts_per_page' => $verito_Options['featured_per_page']
                  
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) : $loop->the_post();
                        verito_products_template();
                    endwhile;
                } else {
                    esc_attr_e('No products found','verito');
                }

                wp_reset_postdata();
                ?>

           </div>
         </div>
      </div>
    </div>
</div>    
</div>
  <?php
  }
}
}

 if ( ! function_exists ( 'verito_bestseller_products' ) ) {
function verito_bestseller_products()
{
   global $verito_Options;

if (isset($verito_Options['enable_home_bestseller_products']) && !empty($verito_Options['enable_home_bestseller_products'])) { 
  ?>


<div class="bestsell-pro">
    <div class="container">
      <div class="slider-items-products">
        <div class="bestsell-block">
          <div id="bestsell-slider" class="product-flexslider hidden-buttons">
            <div class="block-title">
              <h2><?php esc_attr_e('Best ', 'verito'); ?><em><?php esc_attr_e('Sellers', 'verito'); ?></em></h2>              
            </div>
           <div class="slider-items slider-width-col4 products-grid block-content">

                  <?php
                
                              $args = array(
                              'post_type'       => 'product',
                              'post_status'       => 'publish',
                              'ignore_sticky_posts'   => 1,
                              'posts_per_page' => $verito_Options['bestseller_per_page'],      
                              'meta_key'        => 'total_sales',
                              'orderby'         => 'meta_value_num',
                              
                              
                              );

                                $loop = new WP_Query( $args );
                             
                                if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();                  
                                verito_bestseller_products_template();
                               
                                endwhile;
                                } else {
                                esc_attr_e( 'No products found', 'verito' );
                                }

                               wp_reset_postdata();
                             
                             
?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>           


 <?php  } ?>
<?php 

}
}

 if ( ! function_exists ( 'verito_bestseller_products_template' ) ) {
function verito_bestseller_products_template()
{
   
  $Verito = new Verito();
  global $product, $yith_wcwl,$post;
    $imageUrl = woocommerce_placeholder_img_src();
   if (has_post_thumbnail())
      $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'verito-product-size-large');
       $product_type = $product->product_type;
            
             
              $sales_price_date_to='';
              $product_type = $product->product_type;

               $hotdeal=get_post_meta($post->ID,'hotdeal_on_home',true);
              $sales_price_to='';
              if(!empty($hotdeal) && $hotdeal!=='no')
              { 
              if($product_type=='variable')
              {
               $available_variations = $product->get_available_variations();
                $variation_id=$available_variations[0]['variation_id'];
                $newid=$variation_id;
              }
              else
              {
                $newid=$post->ID;
           
              } 

               $sales_price_to = get_post_meta($newid, '_sale_price_dates_to', true);
                $curdate=date("m/d/y h:i:s A");  
            
               if(!empty($sales_price_to))
               {
                $sales_price_date_to = date("Y/m/d", $sales_price_to);
               } 
               else{
                $sales_price_date_to='';
              } 
            }
   ?> 
<div class="item">
    <div class="item-inner">
        <div class="item-img">
          <div class="item-img-info">
              <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                                <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                                 </figure>
                </a>
                     <?php if ($product->is_on_sale()) : ?>
                       <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'verito'); ?>
                       </div>
                      <?php endif; ?>

                  <?php if(!empty($hotdeal) && !empty($sales_price_to))
                    {?>
                       <div class="box-timer">
                          <div class="countbox_1 timer-grid"  data-time="<?php echo esc_html($sales_price_date_to) ;?>">
                          </div>
                       </div>

                  <?php }?>


          <div class="box-hover">
            <ul class="add-to-links">
                          <li>

                            <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                                     
                                           <a class="detail-bnt yith-wcqv-button link-quickview" 
                                           data-product_id="<?php echo esc_html($product->id);?>"></a>
                                    
                            <?php } ?>

                          <li>
                            <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                     $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="link-wishlist"' : 'class="link-wishlist"';
                                    ?>
                                      <a title="<?php esc_attr_e('Add to Wishlist','verito');?>" href="<?php echo esc_url($yith_wcwl->get_addtowishlist_url()) ?>"  data-product-id="<?php echo esc_html($product->id); ?>" data-product-type="<?php echo esc_html($product->product_type); ?>" <?php echo htmlspecialchars_decode($classes); ?>></a> 
                              
                            <?php } ?>
                         </li>
                          <li>
                            
                            <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                           $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare','verito');?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->id)); ?>" class="link-compare add_to_compare compare " data-product_id="<?php echo esc_html($product->id); ?>"></a>
                               
                            <?php } ?> 
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

   <div class="item-info">
     <div class="info-inner">

        <div class="item-title"> 
          <a href="<?php the_permalink(); ?>"
              title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a> 
        </div>
                      
        <div class="item-content">
          
          <div class="rating">
             <div class="ratings">
               <div class="rating-box">
                <?php $average = $product->get_average_rating(); ?>
                  <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> </div>
                </div>
              </div>
           </div>

          <div class="item-price">
            <div class="price-box">
                 <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
            </div>    
          </div>
            
         <div class="action">
             <?php verito_woocommerce_product_add_to_cart_text() ;?> 
         </div>
        
       </div>
    </div>
  </div>
</div>
</div>




<?php

}
}

 if ( ! function_exists ( 'verito_featured_products_template' ) ) {
function verito_featured_products_template()
{
  
  $Verito = new Verito();
  global $product,$yith_wcwl,$post;
    $imageUrl = woocommerce_placeholder_img_src();
   if (has_post_thumbnail())
      $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'verito-product-size-large');
   
   ?> 
           
  <div class="item">
      <div class="item-inner">
         <div class="item-img">
              <div class="item-img-info">
                  <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                    <figure class="img-responsive">
                      <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                    </figure>
                   </a>

                        
                        <?php if ($product->is_on_sale()) : ?>

                           <?php echo apply_filters('woocommerce_sale_flash', ' <div class="sale-label sale-top-left">' . esc_html__('Sale', 'woocommerce') . '</div>', $post, $product); ?>

                       <?php endif; ?>
                              <div class="item-box-hover">
                                <div class="box-inner">

                                      
                                    <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                                      <div class="product-detail-bnt">
                                           <a class="button detail-bnt yith-wcqv-button link-quickview" 
                                           data-product_id="<?php echo esc_html($product->id);?>"><span><?php esc_attr_e('Quick View', 'verito'); ?></span></a>
                                    </div>
                                    <?php } ?>

                                  <div class="actions"><span class="add-to-links">


                                  <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                     $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="link-wishlist"' : 'class="link-wishlist"';
                                    ?>
                                      <a title="<?php esc_attr_e('Add to Wishlist','verito');?>" href="<?php echo esc_url($yith_wcwl->get_addtowishlist_url()) ?>"  data-product-id="<?php echo esc_html($product->id); ?>" data-product-type="<?php echo esc_html($product->product_type); ?>" <?php echo htmlspecialchars_decode($classes); ?>><span><?php esc_attr_e('Add to Wishlist', 'verito'); ?></span></a> 
                              
                                        <?php } ?>


                                       <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                           $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare','verito');?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->id)); ?>" class="compare link-compare add_to_compare" data-product_id="<?php echo esc_html($product->id); ?>"></a>
                               
                                        <?php } ?> 

                                 </span> </div>

                                  <div class="add_cart">
                                      <?php verito_woocommerce_product_add_to_cart_text() ;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                               <a href="<?php the_permalink(); ?>"
                               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                             </div>
                              <div class="item-content">
                                <div class="rating">
                                  <div class="ratings">
                                    <div class="rating-box">
                                       <?php $average = $product->get_average_rating(); ?>
                                       <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> </div>
                                    </div>
                                  </div>
                                 </div>
                                <div class="item-price">
                                  <div class="price-box">
                                   <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                      


<?php

}
}


 if ( ! function_exists ( 'verito_new_products_template' ) ) {
function verito_new_products_template()
{
  
  $Verito = new Verito();
  global $product,$yith_wcwl,$post,$verito_Options;
  $totalproduct=$verito_Options['new_products_per_page'];
    $imageUrl = woocommerce_placeholder_img_src();
   if (has_post_thumbnail())
      $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'verito-product-size-large');
   ?> 
  
<li class="item col-lg-3 col-md-3 col-sm-3 col-xs-6">
    <div class="item-inner">
        <div class="item-img">
          <div class="item-img-info">
              <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                                <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                                 </figure>
                </a>
                      <?php if ($product->is_on_sale()) : ?>
                       <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'verito'); ?>
                       </div>
                      <?php endif; ?>
          <div class="box-hover">
            <ul class="add-to-links">
                          <li>

                            <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                                     
                                           <a class="detail-bnt yith-wcqv-button link-quickview" 
                                           data-product_id="<?php echo esc_html($product->id);?>"></a>
                                    
                            <?php } ?>

                          <li>
                            <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                     $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="link-wishlist"' : 'class="link-wishlist"';
                                    ?>
                                      <a title="<?php esc_attr_e('Add to Wishlist','verito');?>" href="<?php echo esc_url($yith_wcwl->get_addtowishlist_url()) ?>"  data-product-id="<?php echo esc_html($product->id); ?>" data-product-type="<?php echo esc_html($product->product_type); ?>" <?php echo htmlspecialchars_decode($classes); ?>></a> 
                              
                            <?php } ?>
                         </li>
                          <li>
                            
                            <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                           $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare','verito');?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->id)); ?>" class="link-compare add_to_compare compare " data-product_id="<?php echo esc_html($product->id); ?>"></a>
                               
                            <?php } ?> 
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

   <div class="item-info">
     <div class="info-inner">

        <div class="item-title"> 
          <a href="<?php the_permalink(); ?>"
              title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a> 
        </div>
                      
        <div class="item-content">

          <div class="rating">
             <div class="ratings">
               <div class="rating-box">
                <?php $average = $product->get_average_rating(); ?>
                  <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> </div>
                </div>
              </div>
           </div>

          <div class="item-price">
            <div class="price-box">
                 <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
            </div>    
          </div>
            
         <div class="action">
             <?php verito_woocommerce_product_add_to_cart_text() ;?> 
         </div>
        
       </div>
    </div>
  </div>
</div>
</li>          
              

<?php

}
}

 if ( ! function_exists ( 'verito_recommended_products' ) ) {
function verito_recommended_products()
{
   global $verito_Options;
?>

<div>
  <div class="container">
    <div class="row">

<?php
if (isset($verito_Options['enable_home_recommended_products']) && !empty($verito_Options['enable_home_recommended_products'])) { 
  ?>

        <div class="col-md-5 col-sm-12 special bestsell-pro">
          <div class="bestsell-block slider-items-products">
            <div id="special" class="product-flexslider hidden-buttons">
              <div class="block-title">
              <h2><?php esc_attr_e('Special', 'verito'); ?> <em><?php esc_attr_e('Products', 'verito'); ?></em></h2>
            </div>
           <div class="slider-items slider-width-col4 products-grid block-content">

            <?php
                $args = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'meta_key' => 'recommended',                 
                    'posts_per_page' => $verito_Options['recommended_per_page']
                  
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) : $loop->the_post();
                        verito_products_template();
                    endwhile;
                } else {
                    esc_attr_e('No products found','verito');
                }

                wp_reset_postdata();
                ?>

            
           </div>
         </div>
       </div>
     </div>  
     <?php  } ?>

   <?php verito_hotdeal_product(); ?>
  

        
      </div>
    </div>
  </div>         


 
<?php 

}
}

 if ( ! function_exists ( 'verito_products_template' ) ) {
function verito_products_template()
{
   
  $Verito = new Verito();
  global $product, $yith_wcwl,$post;
    $imageUrl = woocommerce_placeholder_img_src();
   if (has_post_thumbnail())
      $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'verito-product-size-large');
   
   ?> 
<div class="item">
    <div class="item-inner">
        <div class="item-img">
          <div class="item-img-info">
              <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                                <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                                 </figure>
                </a>
                      <?php if ($product->is_on_sale()) : ?>
                       <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'verito'); ?>
                       </div>
                      <?php endif; ?>
          <div class="box-hover">
            <ul class="add-to-links">
                          <li>

                            <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                                     
                                           <a class="detail-bnt yith-wcqv-button link-quickview" 
                                           data-product_id="<?php echo esc_html($product->id);?>"></a>
                                    
                            <?php } ?>

                          <li>
                            <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                     $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="link-wishlist"' : 'class="link-wishlist"';
                                    ?>
                                      <a title="<?php esc_attr_e('Add to Wishlist','verito');?>" href="<?php echo esc_url($yith_wcwl->get_addtowishlist_url()) ?>"  data-product-id="<?php echo esc_html($product->id); ?>" data-product-type="<?php echo esc_html($product->product_type); ?>" <?php echo htmlspecialchars_decode($classes); ?>></a> 
                              
                            <?php } ?>
                         </li>
                          <li>
                            
                            <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                           $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare','verito');?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->id)); ?>" class="link-compare add_to_compare compare " data-product_id="<?php echo esc_html($product->id); ?>"></a>
                               
                            <?php } ?> 
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

   <div class="item-info">
     <div class="info-inner">

        <div class="item-title"> 
          <a href="<?php the_permalink(); ?>"
              title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a> 
        </div>
                      
        <div class="item-content">

          <div class="rating">
             <div class="ratings">
               <div class="rating-box">
                <?php $average = $product->get_average_rating(); ?>
                  <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> </div>
                </div>
              </div>
           </div>

          <div class="item-price">
            <div class="price-box">
                 <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
            </div>    
          </div>
            
         <div class="action">
             <?php verito_woocommerce_product_add_to_cart_text() ;?> 
         </div>
        
       </div>
    </div>
  </div>
</div>
</div>




<?php

}
}


if ( ! function_exists ( 'verito_related_upsell_template' ) ) {
function verito_related_upsell_template()
{
  $Verito = new Verito();
 global $product, $yith_wcwl,$post;


$imageUrl = woocommerce_placeholder_img_src();
if (has_post_thumbnail())
    $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'verito-product-size-large');  
?>
        
  <div class="item">
    <div class="item-inner">
        <div class="item-img">
          <div class="item-img-info">
              <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                                <figure class="img-responsive">
                                <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                                 </figure>
                </a>
                      <?php if ($product->is_on_sale()) : ?>
                       <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'verito'); ?>
                       </div>
                      <?php endif; ?>
          <div class="box-hover">
            <ul class="add-to-links">
                          <li>

                            <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                                     
                                           <a class="detail-bnt yith-wcqv-button link-quickview" 
                                           data-product_id="<?php echo esc_html($product->id);?>"></a>
                                    
                            <?php } ?>

                          <li>
                            <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {

                                     $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="link-wishlist"' : 'class="link-wishlist"';
                                    ?>
                                      <a title="<?php esc_attr_e('Add to Wishlist','verito');?>" href="<?php echo esc_url($yith_wcwl->get_addtowishlist_url()) ?>"  data-product-id="<?php echo esc_html($product->id); ?>" data-product-type="<?php echo esc_html($product->product_type); ?>" <?php echo htmlspecialchars_decode($classes); ?>></a> 
                              
                            <?php } ?>
                         </li>
                          <li>
                            
                            <?php if (class_exists('YITH_Woocompare_Frontend')) {
                                           $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>

                                        <a title="<?php esc_attr_e('Add to Compare','verito');?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->id)); ?>" class="link-compare add_to_compare compare " data-product_id="<?php echo esc_html($product->id); ?>"></a>
                               
                            <?php } ?> 
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

   <div class="item-info">
     <div class="info-inner">

        <div class="item-title"> 
          <a href="<?php the_permalink(); ?>"
              title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a> 
        </div>
                      
        <div class="item-content">

          <div class="rating">
             <div class="ratings">
               <div class="rating-box">
                <?php $average = $product->get_average_rating(); ?>
                  <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> </div>
                </div>
              </div>
           </div>

          <div class="item-price">
            <div class="price-box">
                 <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
            </div>    
          </div>
            
         <div class="action">
             <?php verito_woocommerce_product_add_to_cart_text() ;?> 
         </div>
        
       </div>
    </div>
  </div>
</div>
</div>

<?php
}
}

if ( ! function_exists ( 'verito_hotdeal_template' ) ) {
function verito_hotdeal_template()
{
$Verito = new Verito();
 global $product, $yith_wcwl,$post,$verito_Options;
   $imageUrl = woocommerce_placeholder_img_src();
   if (has_post_thumbnail())
        $imageUrl = wp_get_attachment_url(get_post_thumbnail_id());

         $product_type = $product->product_type;
            
              if($product_type=='variable')
              {
               $available_variations = $product->get_available_variations();
               $variation_id=$available_variations[0]['variation_id'];
               $newid=$variation_id;
              }
              else
              {
                $newid=$post->ID;
           
              }                                    
               $sales_price_to = get_post_meta($newid, '_sale_price_dates_to', true);  
               if(!empty($sales_price_to))
               {
               $sales_price_date_to = date("Y/m/d", $sales_price_to);
               } 
               else{
                $sales_price_date_to='';
              } 
               $curdate=date("m/d/y h:i:s A"); 

?> 
 
              
<div class="col-md-7 col-sm-12">
  <div class="offer-slider parallax parallax-2">
    <div>
        <h2> 
          <?php echo htmlspecialchars_decode($verito_Options['home_daily_deal_title']);?>
        </h2>
          <div class="starSeparator"></div>

            <p> <span><?php echo esc_html($post->post_title);?>
                <?php esc_attr_e(':','verito');?></span>
                <?php $excerpt = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
                  echo substr(wp_strip_all_tags($excerpt),0,100);?>
            </p>

            <div class="box-timer">
                  <div class="countbox_2 timer-grid"  data-time="<?php echo esc_html($sales_price_date_to) ;?>">
                </div>
            </div>
          <a href="<?php echo !empty($verito_Options['daily_deal_url']) ? esc_url($verito_Options['daily_deal_url']) : '#' ?>" title="<?php echo esc_html($post->post_title); ?>" class="shop-now">
            <?php esc_attr_e('Shop now','verito'); ?>
          </a>
    </div>
  </div>
</div>            

<?php

}
}


if ( ! function_exists ( 'verito_footer_brand_logo' ) ) {
function verito_footer_brand_logo()
  {
    global $verito_Options;
    if (isset($verito_Options['enable_brand_logo']) && $verito_Options['enable_brand_logo'] && !empty($verito_Options['all-company-logos'])) : ?>
    
<div class="brand-logo">
    <div class="container">
      <div class="slider-items-products">
        <div id="brand-logo-slider" class="product-flexslider hidden-buttons">
          <div class="slider-items slider-width-col6"> 
           
            <!-- Item -->
            
                   <?php foreach ($verito_Options['all-company-logos'] as $_logo) : ?>
                  <div class="item">
                    <a href="<?php echo esc_url($_logo['url']); ?>" target="_blank"> <img
                        src="<?php echo esc_url($_logo['image']); ?>" 
                        alt="<?php echo esc_attr($_logo['title']); ?>"> </a>
                  </div>
                  <?php endforeach; ?>
                
                </div>
            </div>
         </div>
   </div>
</div>
    
  <?php endif;
  }
}



if ( ! function_exists ( 'verito_home_blog_posts' ) ) {
function verito_home_blog_posts()
{
    $count = 0;
    global $verito_Options;
    $Verito = new Verito();
    if (isset($verito_Options['enable_home_blog_posts']) && !empty($verito_Options['enable_home_blog_posts'])) {
        ?>

<div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="blog-outer-container">
          <div class="blog-inner">
            <div class="row">
        <?php

        $args = array('posts_per_page' => 2, 'post__not_in' => get_option('sticky_posts'));
        $the_query = new WP_Query($args);
           $i=1;  
        if ($the_query->have_posts()) :
            while ($the_query->have_posts()) : $the_query->the_post(); ?> 
          
          <div class="col-lg-6 col-md-6 col-sm-6 blog-preview_item">
                <div class="entry-thumb image-hover2">
                    <a href="<?php the_permalink(); ?>">
                                <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail'); ?>
                                <img src="<?php echo esc_url($image[0]); ?>" alt="<?php the_title(); ?>">
                      </a>
                  </div>

                <div class="blog-preview_info">

                  <ul class="post-meta">
                    <li>
                      <i class="fa fa-user"></i><?php esc_attr_e('posted by','verito'); ?> <a href="<?php comments_link(); ?>"><?php the_author(); ?></a> 
                    </li>

                    <li>
                      <i class="fa fa-comments"></i><a href="<?php comments_link(); ?>"><?php comments_number('0', '1 ', '% '); ?><?php esc_attr_e('Comments','verito'); ?></a>
                   </li>

                    <li>
                      <i class="fa fa-calendar"></i>
                       <time datetime="2015-10-04T07:17:52+00:00" class="entry-date published"><?php esc_html(the_time('F d, Y')); ?></time>
                    </li>
                  </ul>
                  
                  <h4 class="blog-preview_title">

                        <a href="<?php the_permalink(); ?>"><?php esc_html(the_title()); ?></a>

                  </h4>
                  

             <div class="blog-preview_desc">
                    <?php the_excerpt(); ?>
                  </div>
                  <a class="blog-preview_btn" href="<?php the_permalink(); ?>"><?php esc_attr_e('READ MORE','verito'); ?></a>
                </div>
              </div>
          

            <?php    $i++;
             endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else: ?>
            <p>
                <?php esc_attr_e('Sorry, no posts matched your criteria.', 'verito'); ?>
            </p>
        <?php endif;
        ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
          

<?php
    }
}
}
