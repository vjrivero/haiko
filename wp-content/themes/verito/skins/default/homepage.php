<script>
	function toggleDistributingForm(){
		if ( document.getElementById("distributing-contact-form").className.match(/(?:^|\s)hidden-form(?!\S)/) ) {
			document.getElementById("distributing-contact-form").className = document.getElementById("distributing-contact-form").className.replace( /(?:^|\s)hidden-form(?!\S)/g , '' );
		}else{
			document.getElementById("distributing-contact-form").className += " hidden-form";
		}
	}
</script>
  
<div class="distributing-contact-form hidden-form" id="distributing-contact-form" onclick="toggleDistributingForm();">
	<div class="distributing-contact-form-content" onclick="toggleDistributingForm();">
		<div class="close-form" onclick="toggleDistributingForm();">X</div>
		<?php if ( function_exists( 'ninja_forms_display_form' ) ) {
		  ninja_forms_display_form( 3 );
		} ?>
	</div>
</div>
<?php verito_home_page_banner(); ?>
<?php verito_bestseller_products(); ?>
<?php verito_recommended_products(); ?>
<?php verito_new_products(); ?>
<?php verito_home_sub_banner(); ?>
<?php verito_featured_products();?>
<?php verito_home_blog_posts();?>
   