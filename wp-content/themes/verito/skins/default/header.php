<!DOCTYPE html>
<html <?php language_attributes(); ?> id="parallax_scrolling">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
 <?php wp_head(); ?>
</head>
<?php
 $Verito = new Verito(); ?>
 
 
<body <?php body_class(); ?>>
<div id="wptime-plugin-preloader"></div>   
<div id="page" class="page">

<header>
    <div class="header-container">

      <div class="header-top">
        <div class="container">
          <div class="row"> 
            <!-- Header Language -->
            <div class="col-xs-12 col-sm-3 col-md-4 col-lg-6">

              <?php echo verito_currency_language(); ?>
              <div class="welcome-msg">  
                 <?php verito_msg(); ?> 
              </div>
            </div>

           <div class="col-xs-6 col-sm-9 col-md-8 col-lg-6 hidden-xs"> 
              <!-- Header Top Links -->
              <?php if( has_nav_menu( 'toplinks' ) )
                  { ?> 
              <div class="toplinks">
                <?php verito_social_media_links(); ?>
                <div class="links">
                  <?php echo verito_top_navigation(); ?>
                </div>
              </div>
              <?php } ?>
              <!-- End Header Top Links --> 
            </div>

          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-2 col-sm-4 col-xs-12 logo-block"> 
           
            <div class="logo"> <?php verito_logo_image();?> </div>
            
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hidden-xs search-wrapper">
           
               <?php echo verito_search_form(); ?>                 
          
                <?php if( has_nav_menu( 'middlelinks' ) )
                  { ?> 

                  <div class="block-company-wrapper">
                      <?php echo verito_middle_navigation(); ?>
                  </div>
             
                <?php } ?>

          </div>

          <div class="col-lg-2 col-md-3 col-sm-2 col-xs-12 top-cart-wrapper"> 
            
              <?php
                    if (class_exists('WooCommerce')) :?>
                       <div class="top-cart-contain"> 
                       <?php verito_mini_cart(); ?>
                      </div>
                    <?php endif;
              ?>

          </div>
        </div>
      </div>

    </div>
    

  </header>


<nav class="navcls">
    <div class="container">
      <div class="mm-toggle-wrap">
          <div class="mm-toggle mobile-toggle"><i class="fa fa-bars"></i><span class="mm-label"><?php esc_attr_e('Menu', 'verito'); ?></span> </div>
        </div>
   
           <?php if(has_nav_menu('main_menu'))
                  { ?> 
        <div class="nav-inner"> 
         <div class="mgk-main-menu">
              <div id="main-menu">
                  <?php echo verito_main_menu(); ?>                        
            </div>
         </div>
      </div>
            <?php  }?>
         
    </div>
</nav>

  <?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
    
      <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <?php woocommerce_breadcrumb(); ?>
          </div> 
        </div>
     
      </div>
    </div>

  <?php endif; ?>
