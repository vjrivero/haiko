<?php 
// call theme skins function
require_once(VERITO_THEME_PATH . '/includes/layout.php');
require_once(VERITO_THEME_PATH . '/core/resize.php');
require_once(VERITO_THEME_PATH . '/includes/mgk_menu.php');
require_once(VERITO_THEME_PATH . '/includes/widget.php');
require_once(VERITO_THEME_PATH . '/includes/mgk_widget.php');
require_once(VERITO_THEME_PATH .'/core/social_share.php');

 /* Include theme variation functions */ 

add_action('init','verito_theme_layouts');
 
 if ( ! function_exists ( 'verito_theme_layouts' ) ) {
 function verito_theme_layouts()
 {
 global $verito_Options;  

if (isset($verito_Options['theme_layout']) && !empty($verito_Options['theme_layout']))
{ 

require_once ( get_template_directory(). '/skins/' . $verito_Options['theme_layout'] . '/functions.php'); 
}
else{

require_once ( get_template_directory(). '/skins/default/functions.php');
}   
 }
}


 /* Include theme variation header */ 

  if ( ! function_exists ( 'verito_theme_header' ) ) {  
   function verito_theme_header()
 {
 global $verito_Options; 

  if (isset($verito_Options['theme_layout']) && !empty($verito_Options['theme_layout']))
{ 
  include(get_template_directory() . '/skins/' . $verito_Options['theme_layout'] . '/header.php');
}
else{

include(get_template_directory() . '/skins/default/header.php');
}

 }
}

/* Include theme variation homepage */ 

  if ( ! function_exists ( 'verito_theme_homepage' ) ) {
  function verito_theme_homepage()
 {  
 global $verito_Options;  

  if (isset($verito_Options['theme_layout']) && !empty($verito_Options['theme_layout']))
{ 
  include(get_template_directory() . '/skins/' . $verito_Options['theme_layout'] . '/homepage.php');
}
else{ 
include(get_template_directory() . '/skins/default/homepage.php');
}
 }
}

 /* Include theme variation footer */ 

 if ( ! function_exists ( 'verito_theme_footer' ) ) {
function verito_theme_footer()
{
 global $verito_Options;   
  if (isset($verito_Options['theme_layout']) && !empty($verito_Options['theme_layout']))
{ 
include(get_template_directory() . '/skins/' . $verito_Options['theme_layout'] . '/footer.php');
  }
else{
include(get_template_directory() . '/skins/default/footer.php');
}
}
}

 /* Include theme  backtotop */
  if ( ! function_exists ( 'verito_backtotop' ) ) {
  function verito_backtotop()
 {  
 global $verito_Options;   
 if (isset($verito_Options['back_to_top']) && !empty($verito_Options['back_to_top'])) {
    ?>
     <script type="text/javascript">
    jQuery(document).ready(function($){ 
        jQuery().UItoTop();
    });
    </script>
<?php
}
 }
}

 /* Include theme  backtotop */
  if ( ! function_exists ( 'verito_layout_breadcrumb' ) ) {
function verito_layout_breadcrumb() {
$Verito = new Verito();
 global $verito_Options; 
?>
 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <?php $Verito->verito_breadcrumbs(); ?>
          </div>
      </div>
    
</div>
</div>
<?php

}
}

  if ( ! function_exists ( 'verito_singlepage_breadcrumb' ) ) {
function verito_singlepage_breadcrumb() {
 $Verito = new Verito();
 global $verito_Options; 

 ?>
  <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <?php $Verito->verito_breadcrumbs(); ?>
          </div>
      </div> 
</div>
</div>
 <?php

}
}

  if ( ! function_exists ( 'verito_simple_product_link' ) ) {
function verito_simple_product_link()
{
  global $product,$class;
  $product_type = $product->product_type;
  $product_id=$product->id;
  if($product->price=='')
  { ?>
<a class="button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
<?php  }
  else{
  ?>
<a class="single_add_to_cart_button add_to_cart_button  product_type_simple ajax_add_to_cart button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>' data-quantity="1" data-product_id="<?php echo esc_attr($product->id); ?>"
      href='<?php echo esc_url($product->add_to_cart_url()); ?>'>
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
<?php
}
}
}

  if ( ! function_exists ( 'verito_allowedtags' ) ) {
function verito_allowedtags() {
    // Add custom tags to this string
        return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<b>,<blockquote>,<strong>,<figcaption>'; 
    }
  }

if ( ! function_exists( 'verito_wp_trim_excerpt' ) ) : 

    function verito_wp_trim_excerpt($wpse_excerpt) {
    $raw_excerpt = $wpse_excerpt;
        if ( '' == $wpse_excerpt ) {

            $wpse_excerpt = get_the_content('');
            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, verito_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

            //Set the excerpt word count and only break after sentence is complete.
                $excerpt_word_count = 75;
                $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
                $tokens = array();
                $excerptOutput = '';
                $count = 0;

                // Divide the string into tokens; HTML tags, or words, followed by any whitespace
                preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

                foreach ($tokens[0] as $token) { 

                    if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
                    // Limit reached, continue until , ; ? . or ! occur at the end
                        $excerptOutput .= trim($token);
                        break;
                    }

                    // Add words to complete sentence
                    $count++;

                    // Append what's left of the token
                    $excerptOutput .= $token;
                }

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

                $excerpt_end = ' '; 
                $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 

                $wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */

            return $wpse_excerpt;   

        }
        return apply_filters('verito_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }

endif; 

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'verito_wp_trim_excerpt');

  if ( ! function_exists ( 'verito_disable_srcset' ) ) {
function verito_disable_srcset( $sources ) {
return false;
}
}
add_filter( 'wp_calculate_image_srcset', 'verito_disable_srcset' );





  if ( ! function_exists ( 'verito_body_classes' ) ) {
function verito_body_classes( $classes ) 
{
  // Adds a class to body.
global $verito_Options; 

$classes[] = 'cms-index-index cms-verito-home';
  return $classes;
}
}
add_filter( 'body_class', 'verito_body_classes');

  if ( ! function_exists ( 'verito_post_classes' ) ) {
function verito_post_classes( $classes ) 
{
  // add custom post classes.
if(class_exists('WooCommerce') && is_woocommerce())
{ 
$classes[]='notblog';
if(is_product_category())
{
 $classes[]='notblog'; 
} 
}
else if(is_category() || is_archive() || is_search() || is_tag() || is_home())
{
$classes[] = 'blog-post container-paper';
}
else
{
$classes[]='notblog';
} 

  return $classes;
}
}
add_filter( 'post_class', 'verito_post_classes');


  if ( ! function_exists ( 'verito_get_link_url' ) ) {
function verito_get_link_url() {
  $has_url = get_url_in_content( get_the_content() );

  return $has_url ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}
}


 //add to cart function
if ( ! function_exists ( 'verito_woocommerce_product_add_to_cart_text' ) ) {
function verito_woocommerce_product_add_to_cart_text() {
    global $product;
    $product_type = $product->product_type;
    $product_id=$product->id;
    if($product->is_in_stock())
    {
    switch ( $product_type ) {
    case 'external':
    ?>
    <a class="button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span> <?php echo esc_html($product->add_to_cart_text()); ?></span>
    </a>
    <?php
       break;
       case 'grouped':
        ?>
    <a class="button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
    <?php
       break;
       case 'simple':
        ?>
    <?php verito_simple_product_link();?>
    <?php
       break;
       case 'variable':
        ?>
    <a class="button btn-cart"  title='<?php esc_attr_e("Select options",'verito'); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span>
    <?php echo esc_html($product->add_to_cart_text()); ?>
    </span> 
    </a>
    <?php
       break;
       default:
        ?>
    <a class="button btn-cart" title='<?php esc_attr_e("Read more",'verito'); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span><?php esc_attr_e('Read more', 'verito'); ?></span> 
    </a>
    <?php
       break;
       
       }
       }
       else
       {
       ?>
    <a type='button' class="button btn-cart" title='<?php esc_attr_e('Out of stock', 'verito'); ?> '
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'
       class='button btn-cart'>
    <span> <?php esc_attr_e('Out of stock', 'verito'); ?> </span>
    </a>
    <?php
    }
}
}

// magik mini cart
if ( ! function_exists ( 'verito_mini_cart' ) ) {
  function verito_mini_cart()
{
    global $woocommerce,$verito_Options;

if (isset($verito_Options['enable_mini_cart']) && !empty($verito_Options['enable_mini_cart'])) {
    ?>

<div class="mini-cart">
     

             <div data-hover="dropdown" class="basket dropdown-toggle">
              <a href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>"> 
                
            <?php 
                 $CartUrl = get_template_directory_uri() . '/images/btn-cart.png';
        
                if (isset($verito_Options['mini_cart_image']['url']) && !empty($verito_Options['mini_cart_image']['url'])) 
                { ?>

                <img class="cart-icon" src="<?php echo esc_url($verito_Options['mini_cart_image']['url']); ?>" alt="<?php esc_attr_e('cart_image','verito'); ?>"> 
                
              <?php  } 
              else { ?>
               <img class="cart-icon" src="<?php echo esc_url($CartUrl) ;?>" alt="<?php esc_attr_e('cart_image','verito'); ?>"> 
             <?php }  ?>

                
                <span class="cart_count"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?></span>
                <span class="price"><?php  esc_attr_e('Cart','verito'); ?> / <?php echo htmlspecialchars_decode(WC()->cart->get_cart_subtotal()); ?></span>
               </a>
              </div>

    <div>     
        <div class="top-cart-content">
                
  
                   
         <?php if (sizeof(WC()->cart->get_cart()) > 0) : $i = 0; ?>
         <ul cclass="mini-products-list" id="cart-sidebar">
            <?php foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) : ?>
            <?php
               $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
               $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
               
               if ($_product && $_product->exists() && $cart_item['quantity'] > 0
                   && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)
               ) :
               
                   $product_name = apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key);
                   $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(array(60, 160)), $cart_item, $cart_item_key);
                   $product_price = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                   $cnt = sizeof(WC()->cart->get_cart());
                   $rowstatus = $cnt % 2 ? 'odd' : 'even';
                   ?>
            <li class="item<?php if ($cnt - 1 == $i) { ?>last<?php } ?>">
              <div class="item-inner">
               <a class="product-image"
                  href="<?php echo esc_url($_product->get_permalink($cart_item)); ?>"  title="<?php echo esc_html($product_name); ?>"> <?php echo str_replace(array('http:', 'https:'), '', htmlspecialchars_decode($thumbnail)); ?> </a>
             

                  <div class="product-details">
                       <div class="access">
                        <a class="btn-edit" title="<?php esc_attr_e('Edit item','verito') ;?>"
                        href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>"><i
                        class="icon-pencil"></i><span
                        class="hidden"><?php esc_attr_e('Edit item','verito') ;?></span></a>
                     <a href="<?php echo esc_url(WC()->cart->get_remove_url($cart_item_key)); ?>"
                        title="<?php esc_attr_e('Remove This Item','verito') ;?>" onClick="" 
                        class="btn-remove1"><?php esc_attr_e('Remove','verito') ;?></a> 

                         </div>
                      <strong><?php echo esc_html($cart_item['quantity']); ?>
                  </strong> x <span class="price"><?php echo htmlspecialchars_decode($product_price); ?></span>
                     <p class="product-name"><a href="<?php echo esc_url($_product->get_permalink($cart_item)); ?>"
                        title="<?php echo esc_html($product_name); ?>"><?php echo esc_html($product_name); ?></a> </p>
                  </div>
                  <?php echo htmlspecialchars_decode(WC()->cart->get_item_data($cart_item)); ?>
                     </div>
              
            </li>

            <?php endif; ?>
            <?php $i++; endforeach; ?>
         </ul> 
         <!--actions-->
                    
         <div class="actions">
                      <button class="btn-checkout" title="<?php esc_attr_e('Checkout','verito') ;?>" type="button" 
                      onClick="window.location.assign('<?php echo esc_js(WC()->cart->get_checkout_url()); ?>')">
                      <span><?php esc_attr_e('Checkout','verito') ;?></span> </button>

                       
                      <a class="view-cart" type="button"
                     onClick="window.location.assign('<?php echo esc_js(WC()->cart->get_cart_url()); ?>')">
                     <span><?php esc_attr_e('View Cart','verito') ;?></span> </a>
                     
          
         </div>   
         
         <?php else:?>
         <p class="a-center noitem">
            <?php esc_attr_e('Sorry, nothing in cart.', 'verito');?>
         </p>
         <?php endif; ?>
      </div>
   </div>
 </div>

<?php
}
}
}


  //social links
if ( ! function_exists ( 'verito_social_media_links' ) ) {
  function verito_social_media_links()
  {
    global $verito_Options;
    if(isset($verito_Options
  ['enable_social_link_footer']) && !empty($verito_Options['enable_social_link_footer']))
    {?>
  <div class="social">
       <h4><?php esc_attr_e('Follow Us', 'verito'); ?></h4>
    <ul class="link">
 
  <?php
    if (isset($verito_Options
  ['social_facebook']) && !empty($verito_Options['social_facebook'])) {
      echo "<li class=\"fb pull-left\"><a target=\"_blank\" href='".  esc_url($verito_Options['social_facebook']) ."'></a></li>";
    }

    if (isset($verito_Options['social_twitter']) && !empty($verito_Options['social_twitter'])) {
      echo "<li class=\"tw pull-left\"><a target=\"_blank\" href='".  esc_url($verito_Options['social_twitter']) ."'></a></li>";
    }

    if (isset($verito_Options['social_googlep']) && !empty($verito_Options['social_googlep'])) {
      echo "<li class=\"googleplus pull-left\"><a target=\"_blank\" href='".  esc_url($verito_Options['social_googlep'])."'></a></li>";
    }

    if (isset($verito_Options['social_rss']) && !empty($verito_Options['social_rss'])) {
      echo "<li class=\"rss pull-left\"><a target=\"_blank\" href='".  esc_url($verito_Options['social_rss'])."'></a></li>";
    }

    if (isset($verito_Options['social_pinterest']) && !empty($verito_Options['social_pinterest'])) {
      echo "<li class=\"pintrest pull-left\"><a target=\"_blank\" href='".  esc_url($verito_Options['social_pinterest'])."'></a></li>";
    }

    if (isset($verito_Options['social_linkedin']) && !empty($verito_Options['social_linkedin'])) {
      echo "<li class=\"linkedin pull-left\"><a target=\"_blank\" href='".  esc_url($verito_Options['social_linkedin'])."'></a></li>";
    }
     if (isset($verito_Options['social_instagram']) && !empty($verito_Options['social_instagram'])) {
      echo "<li class=\"instagram pull-left\"><a target=\"_blank\" href='".  esc_url($verito_Options['social_instagram'])."'></a></li>";
    }
    if (isset($verito_Options['social_youtube']) && !empty($verito_Options['social_youtube'])) {
      echo "<li class=\"youtube pull-left\"><a target=\"_blank\" href='".  esc_url($verito_Options['social_youtube'])."'></a></li>";
    }
    ?>
 </ul>
 </div>

 <?php }
  }
}

 

  // bottom copyright text 
if ( ! function_exists ( 'verito_footer_text' ) ) {
  function verito_footer_text()
  {
    global $verito_Options;
    if (isset($verito_Options['bottom-footer-text']) && !empty($verito_Options['bottom-footer-text'])) {?>
      <div class="footer-bottom">
      <div class="container">
        <div class="row">
      <?php echo htmlspecialchars_decode ($verito_Options['bottom-footer-text']);?>
      </div>
      </div>
    </div>
    <?php
    }
  }
}


?>