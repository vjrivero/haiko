<?php
/* Add your custom functions here */
add_action( 'wp_enqueue_scripts', 'magikVerito_child_theme_enqueue_styles', 1000 );
function magikVerito_child_theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'magikVerito-style' ) );
}
?>
