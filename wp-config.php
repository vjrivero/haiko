<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'haikowp_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|j*1]aqV|aZ[Me|hut?DfZ^LKGDlhPe,`OIb)T8lxy>j/~OjQ=b`L`W K/;}2qv.');
define('SECURE_AUTH_KEY',  'Jaogp<7i2v~,/2f@4L6Y3(3OLty:l;Y3Z/zc`5K-c1kpo`mNY()x$$!mhSb*{MQT');
define('LOGGED_IN_KEY',    'k3%QNtpIXpB3pin:uhM^PCbqn/Ar_,%$b*g,^bcznS,F3Q%H$Sk2R[/UyY3c^~}k');
define('NONCE_KEY',        'aVuf;-qfKzU~9^Mtz:/xqan>c>L)z!W$ZD*V]Df`>^5g:8ONP2G;K#.>K91$jTY!');
define('AUTH_SALT',        'E*P4r4oYOE0JNdE6(#siZJp!T9Q2HQ/Z@p?DeY`7}BSj/L*i&EMN|vd/|*zO%*Fn');
define('SECURE_AUTH_SALT', 'J/br:spBx;fkXTWFd(TmKZcV{fAkCbsfUY$~XWC=Gr#.+ha6W-HQK^#`ib}O<4Mi');
define('LOGGED_IN_SALT',   'XWCAB.0mL6k!j7C(DB2+3uxGT2,5`i!BT%9~[OKd@[/??6`@+ulnbt-`Z^b?*9wN');
define('NONCE_SALT',       '7w1v9j23#`?_,_5z.}p|d5VLI_YcM0N.5[}P-L J&9oO,%g>j5mhse4l}O86!s%f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
